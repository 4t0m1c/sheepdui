﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class achievement
{
	public string Title { get; set; }
	public string Description { get; set; }
	public int Reward { get; set; }
	public bool SheepReward { get; set; }
	public bool Achieved { get; set; }
	
}

public class Achievements_Database : MonoBehaviour {

	public UI_Controller _UI_Controller;
	public GameObject AchievePrefab;
	public List<achievement> Achievements;
	public List<GameObject> Achieved;
	GameObject achievementPar;

	// Use this for initialization
	void Awake () {

		//Database of achievements
		Achieved = new List<GameObject>();
		Achievements = new List<achievement> ();	
		Achievements.Add (new achievement () {Title="Adept Farmer",			
			Description="Play for 1 hour",
			Reward=250,
			SheepReward=false,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Apprentice Farmer",	
			Description="Play for 15 min",
			Reward=100,
			SheepReward=false,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Expert Farmer",		
			Description="Play for 3 hours",
			Reward=500,
			SheepReward=false,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Bombs Away I",			
			Description="Drop 1 sheep off the island",
			Reward=25,
			SheepReward=false,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Bombs Away II",		
			Description="Drop 10 sheep off the island",
			Reward=100,
			SheepReward=false,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Bombs Away III",		
			Description="Drop 50 sheep off the island",
			Reward=500,
			SheepReward=false,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Do You Even Lift",		
			Description="Get a wolf killed by a weight",
			Reward=400,
			SheepReward=false,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Get Cooking",			
			Description="10 sheep incinerated by the dragon",
			Reward=5,
			SheepReward=true,
			Achieved=false});
		Achievements.Add (new achievement () {Title="In The Beginning",		
			Description="Complete the tutorial level",
			Reward=5,
			SheepReward=true,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Master Farmer",		
			Description="Play for 10 hours",
			Reward=5000,
			SheepReward=false,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Master Grass Cutter",	
			Description="Eat all the grass on the island",
			Reward=2500,
			SheepReward=false,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Novice Farmer",		
			Description="Play for 5 min",
			Reward=25,
			SheepReward=false,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Sheep Hearder I",		
			Description="Have at least 10 sheep",
			Reward=2,
			SheepReward=true,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Sheep Hearder II",		
			Description="Have at least 20 sheep",
			Reward=300,
			SheepReward=false,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Sheep Hearder III",	
			Description="Have at least 30 sheep",
			Reward=1000,
			SheepReward=false,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Untouchable I",		
			Description="Make it through any level from 5-10 without injuries",
			Reward=150,
			SheepReward=false,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Untouchable II",		
			Description="Make it through any level from 15-20 without injuries",
			Reward=300,
			SheepReward=false,
			Achieved=false});
		Achievements.Add (new achievement () {Title="Untouchable III",		
			Description="Make it through any level from 25-30 without injuries",
			Reward=600,
			SheepReward=false,
			Achieved=false});

	}

	void Start(){
		//TESTING ONLY
		PlayerPrefs.SetInt("In The Beginning", 1);

		achievementPar = _UI_Controller.Menu_Achievements.transform.GetChild(0).GetChild(0).gameObject;
		for (int i = 0; i<Achievements.Count; i++){
			GameObject newAchievement = Instantiate (AchievePrefab, Vector3.zero, Quaternion.identity) as GameObject;
			newAchievement.transform.SetParent(achievementPar.transform); //Set parent
			newAchievement.transform.localScale = new Vector3 (1,1,1);
			newAchievement.transform.GetChild(1).GetComponent<Text>().text = Achievements[i].Title; //Title
			newAchievement.transform.GetChild(2).GetComponent<Text>().text = Achievements[i].Description; //Description
			newAchievement.transform.GetChild(3).GetComponent<Text>().text = Achievements[i].Reward.ToString(); //Reward
			newAchievement.transform.GetChild(4).gameObject.SetActive(Achievements[i].SheepReward); //RewardType
			if (PlayerPrefs.GetInt(Achievements[i].Title, 0) == 1){Achievements[i].Achieved = true;}
			if (Achievements[i].Achieved == true){ //Achieved
				newAchievement.transform.GetChild(5).GetChild(0).gameObject.SetActive(true); 
				newAchievement.GetComponent<Image>().color = new Color(0.455f,0.455f,0.455f);
				Achieved.Add(newAchievement);
			} else {
				newAchievement.transform.GetChild(5).GetChild(0).gameObject.SetActive(false);
				newAchievement.transform.GetChild(5).GetChild(1).gameObject.SetActive(true);
			}
		}
		for (int i = 0; i < Achieved.Count; i++){
			Achieved[i].transform.SetAsLastSibling();
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
