﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SheepCount : MonoBehaviour {
	public Text text;

	public int currentsheepcount = 0;
	public int FlockCount = 0;

	public void Start () {
		//text.text = currentsheepcount.ToString();
		FlockCount = PlayerPrefs.GetInt ("FlockCount",-1);
		if (FlockCount == -1){
			FlockCount = 1;
			PlayerPrefs.SetInt ("FlockCount",FlockCount);
		}

	}

	public int getCount () {
		return currentsheepcount;
	}

	public void addSheep () {
		currentsheepcount += 1;
		text.text = "" + currentsheepcount;
	}

	public void removeSheep () {
		currentsheepcount -= 1;
		FlockCount -= 1;
		text.text = "" + currentsheepcount;
	}
}
