﻿using UnityEngine;
using System.Collections;

public class MoveController : MonoBehaviour {
	public float NoticeDistance = 5;
	public GameObject SheepPrefab;
	
	private Camera mainCamera;
	
	private Vector3 TouchStartPos;
	private Vector3 TouchEndPos;
	
	private bool moved = false;
	public bool addS = false;

	RaycastHit PositionRaycast(Vector2 touchPos) {
		int layermask = ~(1 << 9);
		Ray ray = mainCamera.ScreenPointToRay(touchPos);
		RaycastHit hit;		
		Physics.Raycast (ray, out hit, (mainCamera.farClipPlane), layermask);
		Debug.DrawRay (ray.origin, ray.direction * mainCamera.farClipPlane,Color.yellow);
		return hit;
	}

	Vector3 MoveRaycast(Vector2 touchPos) {
		int layermask = 1 << 9;
		Ray ray = mainCamera.ScreenPointToRay (touchPos);
		RaycastHit hit;		
		Physics.Raycast (ray, out hit, (mainCamera.farClipPlane), layermask);
		//Debug.DrawRay (ray.origin, ray.direction * mainCamera.farClipPlane,Color.red);
		if (hit.transform == null) {
			return Vector3.up;
		} else {
			return hit.point;
		}
	}

	public void Started(Vector2 touchPos, bool isUI) {
		if (!isUI) {
			TouchStartPos = MoveRaycast (touchPos);
		}
	}

	public void Moved(Vector2 touchPos, bool isUI) {
		if (!isUI) {
			TouchEndPos = MoveRaycast (touchPos);
			if (TouchEndPos != Vector3.up) {
				Vector3 newPos = TouchEndPos - TouchStartPos;
				transform.Translate (new Vector3 (-newPos.x, 0, -newPos.z));
				if (newPos.magnitude > 0.015) {
					moved = true;
				}
			} else {
				// Do nothing
			}
		}
	}

	public void Ended(Vector2 touchPos, bool isUI) {
		if (!isUI) {
			if (!moved && !addS) {
				CommandSheep (touchPos);
			} else {
				AddSheep(touchPos);
			}
			moved = false;
		}
	}

	public void CommandSheep (Vector2 pos) {
		Transform hitTransform = PositionRaycast (pos).transform;
		GameObject[] Sheepz = GameObject.FindGameObjectsWithTag ("Sheep");
		float nearest = Mathf.Infinity;
		Transform target = null;
		if (Sheepz.Length != 0 && hitTransform != null) {
			for (int i = 0; i < Sheepz.Length; i++) {
				Transform o = Sheepz [i].transform;
				float distance = Vector3.Distance (hitTransform.position, o.position);
				if (distance < nearest) {
					target = o;
					nearest = distance;
				}
			}
		}
		if (target != null) {
			if (nearest < NoticeDistance && nearest > 0.5f) {
				target.GetComponent<Sheep> ().setGuided (hitTransform.position);
			} else {
				if (nearest < 0.5f) {
					target.GetComponent<Sheep> ().DisplayStats();
				}
			}
		}
	}

	public void AddSheep (Vector2 pos) {
		if (addS) {
			Transform hitTransform = PositionRaycast (pos).transform;
			if (hitTransform.name.Substring(0,5) == "Voxel") {
				GameObject.Instantiate (SheepPrefab, hitTransform.position, Quaternion.Euler (0, 0, 0));
				addS = false;
			}
		}
	}

	public bool getAddS () {
		return addS;
	}

	// Use this for initialization
	void Start () {
		mainCamera = Camera.main;
	}

	public void ShouldAddSheep() {
		addS = true;
	}

	// Usefull but not used
	public Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles) {
		Vector3 dir = point - pivot; // get point direction relative to pivot
		dir = Quaternion.Euler(angles) * dir; // rotate it
		point = dir + pivot; // calculate rotated point
		return point; // return it
	}
}
