﻿using UnityEngine;
using System.Collections;

public class CloudMove : MonoBehaviour {
	private int Distance;
	private Transform CloudRoot;
	private SpawnClouds cloudCreator;

	public float Speed = 0.2f;

	void Start () {
		CloudRoot = transform.parent.transform;
		cloudCreator = CloudRoot.GetComponent<SpawnClouds>();
		Distance = (cloudCreator.Height/ 2) * 10;
	}

	void Update () {
		transform.Translate(Vector3.right * Time.deltaTime * Speed);

		if (Vector3.Distance(transform.position, CloudRoot.position) > Distance && transform.localPosition.x > 0) {
			cloudCreator.SpawnRow(0);
			DestroyImmediate(this.gameObject);
		}
	}
}
