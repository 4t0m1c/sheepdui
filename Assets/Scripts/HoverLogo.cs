﻿using UnityEngine;
using System.Collections;

public class HoverLogo : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Moveup();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Moveup (){
		iTween.MoveBy (this.gameObject, iTween.Hash("name","moveup","amount",new Vector3 (0, 10, 0),"time", 5f,"oncomplete","Movedown", "easetype", "easeinoutsine"));
	}

	public void Movedown (){
		iTween.StopByName("moveup");
		iTween.MoveBy (this.gameObject, iTween.Hash("name","movedown","amount",new Vector3 (0, -20, 0),"time", 10f,"oncomplete","Moveup2", "easetype", "easeinoutsine"));
	}

	public void Moveup2 (){
		iTween.StopByName("movedown");
		iTween.MoveBy (this.gameObject, iTween.Hash("name","moveup","amount",new Vector3 (0, 20, 0),"time", 10f,"oncomplete","Movedown", "easetype", "easeinoutsine"));
	}
}
