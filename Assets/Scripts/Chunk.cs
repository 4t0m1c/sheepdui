﻿using UnityEngine;
using System.Collections;

public class Chunk : MonoBehaviour {
	private ChunkManager Manager;

	public GameObject VoxelPrefab;
	private Transform TerrainObjects;
	private Transform TrapObjects;
	private Transform Voxels;

	private float Size;
	private int Resolution;
	private float HeightMultiplier;
	private float PerlinScale;
	private int Seed;
	private int TerrainSpawnDensity = 100;
	private int TrapSpawnDensity = 100;
	
	private float voxelSize;

	public void ActivateChunk (float size, int resolution, float heightMultiplier, float perlinScale, int seed, int terrainSpawnDensity, int trapSpawnDensity, ChunkManager manager) {
		Size = size;
		Resolution = resolution;
		HeightMultiplier = heightMultiplier;
		PerlinScale = perlinScale;
		Seed = seed;
		Manager = manager;
		TerrainSpawnDensity = terrainSpawnDensity;
		TrapSpawnDensity = trapSpawnDensity;

		TerrainObjects = transform.GetChild(0);
		TrapObjects = transform.GetChild(1);
		Voxels = transform.GetChild(2);

		CreateChunk ();
	}

	private void  CreateChunk() {
		voxelSize = Size / Resolution;
		Random.seed = Seed;

		for (int z = 0; z < Resolution; z++) {
			for (int x = 0; x < Resolution; x++) {
				int r1 = Random.Range(0, 100);
				int r2 = Random.Range(0, 100);
				SpawnVoxel(x, z, r1, r2);
			}
		}
		Destroy (this);
	}
	
	private void SpawnVoxel (int x, int z, int random1, int random2) {
		float perlinX = ((transform.localPosition.x / Size) * Resolution) + x + 1;
		float perlinY = ((transform.localPosition.z / Size) * Resolution) + z + 1;
		float height = Mathf.PerlinNoise ((perlinX + Seed.GetHashCode()) / PerlinScale, (perlinY + Seed.GetHashCode()) / PerlinScale);
		Vector3 pos = new Vector3 ((-Size/2 + x + 0.5f) * voxelSize, (height * HeightMultiplier), (-Size/2 + z + 0.5f) * voxelSize);

		GameObject o = Instantiate (VoxelPrefab) as GameObject;
		o.name = "Voxel";
		o.transform.parent = Voxels;
		o.transform.localPosition = pos;
		o.transform.localScale = Vector3.one * voxelSize;

		if (random1 < TerrainSpawnDensity) {
			GameObject TerrainObject = Instantiate(Manager.RandomTerrainObject());
			TerrainObject.transform.parent = TerrainObjects;
			TerrainObject.transform.localPosition = pos;
			TerrainObject.transform.localScale = Vector3.one * voxelSize;
		} else {
			if (random2 < TrapSpawnDensity) {
				GameObject TrapObject = Instantiate (Manager.RandomTrapObject ());
				TrapObject.transform.parent = TrapObjects;
				TrapObject.transform.localPosition = pos;
				TrapObject.transform.localScale = Vector3.one * voxelSize;
			}
		}
	}
}