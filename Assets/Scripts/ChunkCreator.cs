﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class ChunkCreator : MonoBehaviour {
	[Range(0, 100)]
	public int RandomFillPercent;

	public int MapWidth;
	public int MapHeight;
	
	public int SafeZoneWidth = 3;
	public int SafeZoneHeight = 3;
	[Range(0, 2)]
	public int SafeZoneCount = 0;

	public GameObject VoxelPrefab;

	private int[,] Map;
	private int[,] SafeZones;

	public ChunkManager Manager;
	public StartEnd startEnd;
	private int Width;
	private int Height;
	private int Seed;

	public class Chunk {
		float ChunkSize;
		float ChunkResolution;
		float ChunkHeightMultiplier;
		float PerlinScale;
		int Seed;
		int TrapSpawnDensity;
		int TerrainSpawnDensity;
		Transform Parent;
		int[,] ChunkMap;
		int[,] SafeZoneMap;

		Vector3 Pos;
		Transform ChunkBase;
		Transform TerrainObjects;
		Transform TrapObjects;
		Transform Voxels;
		Transform Air;
		Transform MoveCollider;

		float voxelSize;
		GameObject VoxelPrefab;
		ChunkManager CManager;
		Transform thisChunk;
		StartEnd sEnd;

		public Chunk(GameObject voxelPrefab, Vector3 pos, float size, int resolution, float heightMultiplier, float perlinScale, int seed, int terrainSpawnDensity, int trapSpawnDensity, Transform parent, int[,] chunkMap, int[,] zones, ChunkManager manager, StartEnd startEnd) {
			sEnd = startEnd;
			Pos = pos;
			VoxelPrefab = voxelPrefab;
			ChunkSize = size;
			ChunkResolution = resolution;
			ChunkHeightMultiplier = heightMultiplier;
			PerlinScale = perlinScale;
			Seed = seed;
			TrapSpawnDensity = trapSpawnDensity;
			TerrainSpawnDensity = terrainSpawnDensity;
			Parent = parent;
			ChunkMap = chunkMap;
			SafeZoneMap = zones;
			CManager = manager;

			voxelSize = ChunkSize / ChunkResolution;
			CreateChunk();
		}

		public void  CreateChunk() {
			Transform managerT = GameObject.Find ("Chunk Manager").transform;
			for (int i =0; i< managerT.childCount; i++) {
				Transform child = managerT.GetChild(i);
				if (child.localPosition == Pos) {
					thisChunk = child;
					break;
				}
			}

			CreateHierarchy ();
			if (thisChunk == null) {
				MoveCollider.transform.localPosition = new Vector3 (0, 5, 0);
				BoxCollider bc = MoveCollider.gameObject.AddComponent<BoxCollider> ();
				bc.center = new Vector3 (0, 0, 0);
				bc.size = new Vector3 (ChunkSize, 0.1f, ChunkSize);
				ChunkBase.transform.localPosition = Pos;
			} else {
				for (int i=0;i < TerrainObjects.childCount; i++){
					Destroy(TerrainObjects.GetChild(i).gameObject);
				}

				for (int i=0;i < TrapObjects.childCount; i++){
					Destroy(TrapObjects.GetChild(i).gameObject);
				}
			}

			UnityEngine.Random.seed = Seed;
			for (int x=0; x<ChunkResolution; x++) {
				for (int y=0; y<ChunkResolution; y++) {
					int r1 = UnityEngine.Random.Range(0, 100);
					int r2 = UnityEngine.Random.Range(0, 100);

					SpawnVoxel(x, y, r1, r2);
				}
			}
		}

		private void CreateHierarchy() {
			if (thisChunk == null) {
				ChunkBase = new GameObject ().transform;
				ChunkBase.parent = Parent;
				ChunkBase.name = "Chunk " + Pos.ToString ();

				TerrainObjects = new GameObject ().transform;
				TerrainObjects.parent = ChunkBase.transform;
				TerrainObjects.name = "TerrainArt";

				TrapObjects = new GameObject ().transform;
				TrapObjects.parent = ChunkBase.transform;
				TrapObjects.name = "Traps";

				Voxels = new GameObject ().transform;
				Voxels.parent = ChunkBase.transform;
				Voxels.name = "Voxels";

				Air = new GameObject ().transform;
				Air.parent = ChunkBase.transform;
				Air.name = "Air";

				MoveCollider = new GameObject ().transform;
				MoveCollider.parent = ChunkBase.transform;
				MoveCollider.name = "MoveCollider";
				MoveCollider.gameObject.layer = LayerMask.NameToLayer ("MoveCollider");
			} else {
				ChunkBase = thisChunk;
				TerrainObjects = ChunkBase.GetChild(0);
				TrapObjects = ChunkBase.GetChild(1);
				Voxels = ChunkBase.GetChild(2);
				Air = ChunkBase.GetChild(3);
				MoveCollider = ChunkBase.GetChild(4);
			}
		}

		private void SpawnVoxel (int x, int z, int random1, int random2) {
			float height = PerlinNoise (x, z);
			Vector3 pos = GetVoxelPos (x, height, z);
			bool SafeZone = false;
			bool SpawnGround = false;

			Transform AirVoxel = Air.FindChild ("AirVoxel (" + pos.x + ", " + pos.z + ")");
			Transform GroundVoxel = Voxels.FindChild ("Voxel (" + pos.x + ", " + pos.z + ")");
			
			if (ChunkMap [x, z] == 1) {
				if (AirVoxel != null) {
					// Re-using old air
					AirVoxel.localPosition = pos;
					AirVoxel.localScale = Vector3.one * voxelSize;
					//AirVoxel.GetComponent<Renderer> ().material.color = Color.cyan;
				} else {
					// Looking for Ground
					if (GroundVoxel != null) {
						Destroy (GroundVoxel.gameObject);
					}
					
					// Creating new Air
					AirVoxel = new GameObject ().transform;
					//AirVoxel = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
					AirVoxel.name = "AirVoxel (" + pos.x + ", " + pos.z + ")";
					AirVoxel.parent = Air;
					AirVoxel.localPosition = pos;
					AirVoxel.localScale = Vector3.one * voxelSize;
					//AirVoxel.GetComponent<Renderer> ().material.color = Color.blue;
				}
			} else {
				if (AirVoxel != null) {
					// Looking for Air and removing
					if (AirVoxel != null) {
						Destroy (AirVoxel.gameObject);
						SpawnGround = true;
					}
				} else {
					// Re-Using Ground
					if (GroundVoxel != null) {
						GroundVoxel.localPosition = pos;
						GroundVoxel.localScale = Vector3.one * voxelSize;
						GroundVoxel.GetComponent<Ground>().Reset();
						GroundVoxel.GetChild (0).GetComponent<SpriteRenderer> ().material.color = Color.white;
						//GroundVoxel.GetChild (0).GetComponent<SpriteRenderer> ().material.color = Color.green;
					} else {
						// Missing Voxel
						SpawnGround = true;
					}
				}
				
				if (SpawnGround) {
					GameObject Obj = Instantiate (VoxelPrefab, Vector3.zero, Quaternion.identity) as GameObject;
					GroundVoxel = Obj.transform;
					GroundVoxel.name = "Voxel (" + pos.x + ", " + pos.z + ")";
					GroundVoxel.parent = Voxels;
					GroundVoxel.localPosition = pos;
					GroundVoxel.localScale = Vector3.one * voxelSize;
					//GroundVoxel.GetChild (0).GetComponent<SpriteRenderer> ().material.color = Color.red;
				}

				if (SafeZoneMap [x, z] == 1) {
					Transform[] start = sEnd.StartZone;
					
					for (int t=0; t<start.Length; t++) {
						if (start [t] == null && SafeZone == false) {
							start [t] = GroundVoxel;
							SafeZone = true;
						} else {
							continue;
						}
					}
					GroundVoxel.GetChild (0).GetComponent<SpriteRenderer> ().material.color = Color.blue;
				} else if (SafeZoneMap [x, z] == 2) {
					Transform[] end = sEnd.EndZone;
					
					for (int t=0; t<end.Length; t++) {
						if (end [t] == null && SafeZone == false) {
							end [t] = GroundVoxel;
							SafeZone = true;
						} else {
							continue;
						}
					}
					GroundVoxel.GetChild (0).GetComponent<SpriteRenderer> ().material.color = Color.red;
				} else if (SafeZoneMap [x, z] == 3) {
					GroundVoxel.GetChild (0).GetComponent<SpriteRenderer> ().material.color = Color.cyan;
					SafeZone = true;
				}
				if (!SafeZone) {
					SpawnSurfaceObject (pos, random1, random2);
				}
			}
		}

		private void SpawnSurfaceObject (Vector3 pos, int random1, int random2) {
			if (random1 < TerrainSpawnDensity) {
				GameObject TerrainObj = Instantiate(CManager.RandomTerrainObject());
				Transform TerrainObject = TerrainObj.transform;
				TerrainObject.parent = TerrainObjects;
				TerrainObject.localPosition = pos;
				TerrainObject.localScale = Vector3.one * voxelSize;
			} else if (random2 < TrapSpawnDensity) {
				GameObject TrapObj = Instantiate (CManager.RandomTrapObject ());
				Transform TrapObject = TrapObj.transform;
				TrapObject.parent = TrapObjects;
				TrapObject.localPosition = pos;
				TrapObject.localScale = Vector3.one * voxelSize;
				TrapObject.name = TrapObject.name.Substring(0, TrapObject.name.Length - 7);
			}
		}

		private Vector3 GetVoxelPos (int x, float height, int z) {
			return new Vector3 ((-ChunkSize/2 + x + 0.5f) * voxelSize, (height * ChunkHeightMultiplier), (-ChunkSize/2 + z + 0.5f) * voxelSize);
		}

		private float PerlinNoise (int x, int z) {
			float perlinX = ((ChunkBase.localPosition.x / ChunkSize) * ChunkResolution) + x + 1;
			float perlinY = ((ChunkBase.localPosition.z / ChunkSize) * ChunkResolution) + z + 1;
			return Mathf.PerlinNoise ((perlinX + Seed.GetHashCode()) / PerlinScale, (perlinY + Seed.GetHashCode()) / PerlinScale);
		}
	}

	public class Island : IComparable<Island> {
		public List<Coord> Voxels;
		public List<Coord> EdgeVoxels;
		public List<Island> connectedIslands;
		public int IslandSize;
		public bool isConnectedToMain;
		public bool isMain;

		public Island () {

		}

		public Island (List<Coord> IslandVoxels, int[,] map) {
			Voxels = IslandVoxels;
			IslandSize = Voxels.Count;
			connectedIslands = new List<Island> ();
			EdgeVoxels = new List<Coord> ();

			foreach (Coord voxel in Voxels) {
				for (int x = voxel.posX - 1; x <= voxel.posX + 1; x++) {
					for (int y = voxel.posY - 1; y <= voxel.posY + 1; y++) {
						if (x == voxel.posX || y == voxel.posY) {
							if (map[x,y] == 0) {
								EdgeVoxels.Add(voxel);
							}
						}
					}
				}
			}
		}

		public static void ConnectIslands(Island IslandA, Island IslandB) {
			if (IslandA.isConnectedToMain) {
				IslandB.SetAccessableMain();
			} else if (IslandB.isConnectedToMain) {
				IslandB.SetAccessableMain();
			}
			IslandA.connectedIslands.Add (IslandB);
			IslandB.connectedIslands.Add (IslandA);
		}

		public bool IsConnected (Island testIsland) {
			return connectedIslands.Contains (testIsland);
		}

		public int CompareTo(Island otherIsland) {
			return otherIsland.IslandSize.CompareTo (IslandSize);
		}

		public void SetAccessableMain () {
			if (!isConnectedToMain) {
				isConnectedToMain = true;
				foreach (Island cIsland in connectedIslands) {
					cIsland.SetAccessableMain();
				}
			}
		}
	}

	public struct Coord {
		public int posX;
		public int posY;
		
		public Coord(int x, int y) {
			posX = x;
			posY = y;
		}
	}

	public void Start() {
		Manager = this.transform.GetComponent<ChunkManager> ();
		Width =  MapWidth * Manager.myResolution;
		Height =  MapWidth * Manager.myResolution;
		Seed = Manager.Seed;
		GenerateMap ();
		Manager.LoadRandomTrapList ();
		SpawnChunks ();
	}

	public void ResetWorld () {
		Width =  MapWidth * Manager.myResolution;
		Height =  MapWidth * Manager.myResolution;
		Seed = Manager.Seed;
		GenerateMap ();

		/*for (int i=0; i < transform.childCount; i++) {
			GameObject.Destroy(transform.GetChild(i).gameObject);
		}*/

		SpawnChunks ();
	}

	private void SpawnChunks() {
		for (int x=0; x < MapWidth; x++) {
			for (int y=0; y < MapHeight; y++) {
				new Chunk(VoxelPrefab, new Vector3((x - MapWidth / 2) * Manager.Size, 0, (y - MapHeight / 2) * Manager.Size), Manager.Size, Manager.myResolution, Manager.HeightMultiplier, Manager.PerlinScale, Seed, Manager.TerrainSpawnDensity, Manager.TrapSpawnDensity, this.transform, GetChunkArray(x, y, Map), GetChunkArray(x, y, SafeZones), Manager, startEnd);
			}
		}
	}

	private void GenerateMap() {
		Map = new int[Width, Height];
		RandomFillMap ();
		SmoothMap ();

		ProcessAir ();
		ProcessLand ();
		CreateZones ();
		ProcessBridges ();
	}

	private void RandomFillMap() {
		UnityEngine.Random.seed = Seed;

		for (int x = 0; x < Width; x++) {
			for (int y = 0; y < Height; y++) {
				if (x == 0 || x == Width - 1 || y == 0 || y == Height - 1) {
					Map[x,y] = 1;
				} else {
					Map[x,y] = (UnityEngine.Random.Range(0, 100) < RandomFillPercent) ? 1 : 0;
				}
			}
		}
	}

	private void SmoothMap() {
		for (int x = 0; x < Width; x++) {
			for (int y = 0; y < Height; y++) {
				int neighbours= GetSurroundingPositiveValueCount(x,y);

				if (neighbours > 4) {
					Map[x,y] = 1;
				} else if (neighbours < 4) {
					Map[x,y] = 0;
				}
			}
		}
	}

	private int GetSurroundingPositiveValueCount(int gridX, int gridY) {
		int PVCount = 0;
		for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++) {
			for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++) {
				if (IsInsideBounds(neighbourX, neighbourY)) {
					if (neighbourX != gridX || neighbourY != gridY) {
						PVCount += Map[neighbourX, neighbourY];
					}
				} else {
					PVCount++;
				}
			}
		}
		return PVCount;
	}

	private int[,] GetChunkArray(int x, int y, int[,] map) {
		int[,] subMap = new int[Manager.myResolution, Manager.myResolution];
		
		for (int subX = 0; subX < Manager.myResolution; subX++) {
			for (int subY = 0; subY < Manager.myResolution; subY++) {
				subMap[subX, subY] = map[subX + (x * Manager.myResolution), subY + (y * Manager.myResolution)];
			}
		}
		return subMap;
	}

	public bool IsInsideBounds(int x, int y) {
		return x >= 0 && x < Width && y >= 0 && y < Height;
	}

	private List<List<Coord>> GetRegions (int voxelType) {
		List<List<Coord>> Regions = new List<List<Coord>> ();
		int[,] MapFlags = new int[Width, Height];

		for (int x = 0; x < Width; x++) {
			for (int y = 0; y < Height; y++) {
				if (MapFlags[x,y] == 0 && Map[x,y] == voxelType) {
					List<Coord> newRegion = GetIsland(x, y);
					Regions.Add(newRegion);

					foreach (Coord voxel in newRegion) {
						MapFlags[voxel.posX, voxel.posY] = 1;
					}
				}
			}
		}

		return Regions;
	}

	private List<Coord> GetIsland (int StartPosX, int StartPosY) {
		List<Coord> Voxels = new List<Coord> ();
		int[,] MapFlags = new int[Width, Height];
		int VoxelType = Map [StartPosX, StartPosY];

		Queue<Coord> queue = new Queue<Coord> ();
		queue.Enqueue (new Coord(StartPosX, StartPosY));
		MapFlags [StartPosX, StartPosY] = 1;

		while (queue.Count > 0) {
			Coord voxel = queue.Dequeue();
			Voxels.Add(voxel);

			for (int x = voxel.posX - 1; x <= voxel.posX + 1; x++) {
				for (int y = voxel.posY - 1; y <= voxel.posY + 1; y++) {
					if (IsInsideBounds(x, y) && (x == voxel.posX || y == voxel.posY)) {
						if (MapFlags[x,y] == 0 && Map[x,y] == VoxelType) {
							MapFlags[x,y] = 1;
							queue.Enqueue(new Coord(x,y));
						}
					}
				}
			}
		}

		return Voxels;
	}

	private void CreateZone (int type, List<Coord> ValidVoxels) {
		UnityEngine.Random.seed = Mathf.RoundToInt(UnityEngine.Random.Range (1111, 9999) * Time.time);
		int RandomVal = UnityEngine.Random.Range(0, ValidVoxels.Count);
		Coord RandomPos = ValidVoxels[RandomVal];
		int count = 0;
		int tries = 0;
		int ZoneWidth = (SafeZoneWidth -1)/2;
		int ZoneHeight = (SafeZoneHeight -1)/2;
		
		int xShift = 0;
		int yShift = 0;
		
		while (true) {
			if (tries > 10) {
				Debug.Log("Create Zone Crashed");
				break;
			}

			count = 0;
			for (int x = ((RandomPos.posX - ZoneWidth) + xShift); x <= ((RandomPos.posX + ZoneWidth)  + xShift); x++) {
				for (int y = ((RandomPos.posY - ZoneHeight) + yShift); y <= ((RandomPos.posY + ZoneHeight) + yShift); y++) {
					if (IsInsideBounds(x,y)) {
						if (Map[x,y] == 0) {
							count += 1;
						}
					} else {
						Debug.Log ("Zone was out of bounds");
						RandomVal = UnityEngine.Random.Range(0, ValidVoxels.Count);
						RandomPos = ValidVoxels[RandomVal];
						count = 0;
						tries += 1;
					}
				}
			}
			if (count == SafeZoneWidth * SafeZoneHeight) {
				break;
			} else {
				tries +=1;
				if (Map[RandomPos.posX - ZoneWidth, RandomPos.posY - ZoneHeight] == 1) {
					xShift += 1;
					yShift += 1;
				} else if (Map[RandomPos.posX - ZoneWidth, RandomPos.posY + ZoneHeight] == 1) {
					xShift += 1;
					yShift -= 1;
				} else if (Map[RandomPos.posX + ZoneWidth, RandomPos.posY - ZoneHeight] == 1) {
					xShift -= 1;
					yShift += 1;
				} else if (Map[RandomPos.posX + ZoneWidth, RandomPos.posY + ZoneHeight] == 1) {
					xShift -= 1;
					yShift -= 1;
				}
			}
		}

		for (int x = ((RandomPos.posX - ZoneWidth) + xShift); x <= ((RandomPos.posX + ZoneWidth)  + xShift); x++) {
			for (int y = ((RandomPos.posY - ZoneHeight) + yShift); y <= ((RandomPos.posY + ZoneHeight) + yShift); y++) {
				if (Map[x,y] == 0) {
					count += 1;
				}
			}
		}
		
		for (int x = ((RandomPos.posX - ZoneWidth) + xShift); x <= ((RandomPos.posX + ZoneWidth)  + xShift); x++) {
			for (int y = ((RandomPos.posY - ZoneHeight) + yShift); y <= ((RandomPos.posY + ZoneHeight) + yShift); y++) {
				SafeZones [x, y] = type;
			}
		}
	}

	private void CreateZones () {
		SafeZones = new int[Width, Height];
		List<Coord> ValidVoxels = new List<Coord> ();
		List<List<Coord>> Regions = GetRegions (0);

		List<Coord> QuadA = new List<Coord> ();
		List<Coord> QuadB = new List<Coord> ();
		List<Coord> QuadC = new List<Coord> ();
		List<Coord> QuadD = new List<Coord> ();
		
		foreach (List<Coord> region in Regions) {
			ValidVoxels.AddRange(region);
		}

		startEnd.StartZone = new Transform[SafeZoneWidth * SafeZoneHeight];
		startEnd.EndZone = new Transform[SafeZoneWidth * SafeZoneHeight];

		foreach (Coord Voxel in ValidVoxels) {
			if (Voxel.posX < Width/2 && Voxel.posY < Height/2) {
				QuadA.Add(Voxel);
			} else if (Voxel.posX > Width/2 && Voxel.posY < Height/2) {
				QuadB.Add(Voxel);
			} else if (Voxel.posX < Width/2 && Voxel.posY > Height/2) {
				QuadC.Add(Voxel);
			} else {
				QuadD.Add(Voxel);
			}
		}

		int StartZonePos = UnityEngine.Random.Range (0, 3);
		switch (StartZonePos) {
		case 0: 
			CreateZone (1, QuadA);
			CreateZone (2, QuadD);
			if (SafeZoneCount == 1) {

			} else if (SafeZoneCount == 2) {
				CreateZone(3, QuadC);
				CreateZone(3, QuadB);
			}
			break;
		case 1:
			CreateZone (1, QuadB);
			CreateZone (2, QuadC);
			if (SafeZoneCount == 1) {
				if (UnityEngine.Random.Range(0,1) == 0) {
					CreateZone(3, QuadA);
				} else {
					CreateZone(3, QuadD);
				}
			} else if (SafeZoneCount == 2) {
				CreateZone(3, QuadA);
				CreateZone(3, QuadD);
			}
			break;
		case 2:
			CreateZone (1, QuadC);
			CreateZone (2, QuadB);
			if (SafeZoneCount == 1) {
				if (UnityEngine.Random.Range(0,1) == 0) {
					CreateZone(3, QuadA);
				} else {
					CreateZone(3, QuadD);
				}
			} else if (SafeZoneCount == 2) {
				CreateZone(3, QuadA);
				CreateZone(3, QuadD);
			}
			break;
		case 3:
			CreateZone (1, QuadD);
			CreateZone (2, QuadA);
			if (SafeZoneCount == 1) {
				if (UnityEngine.Random.Range(0,1) == 0) {
					CreateZone(3, QuadC);
				} else {
					CreateZone(3, QuadB);
				}
			} else if (SafeZoneCount == 2) {
				CreateZone(3, QuadC);
				CreateZone(3, QuadB);
			}
			break;
		}
	}

	private void ProcessAir () {
		Profiler.BeginSample ("Process Air");
		List<List<Coord>> AirRegions = GetRegions (0);
		List<Island> Islands = new List<Island> ();
		
		int IslandSize = 60;
		foreach (List<Coord> region in AirRegions) {
			if (region.Count < IslandSize) {
				foreach (Coord voxel in region) {
					Map[voxel.posX, voxel.posY] = 1;
				}
			} else {
				Islands.Add(new Island(region, Map));
			}
		}
		Profiler.EndSample ();
	}

	private void ProcessLand () {
		Profiler.BeginSample ("Process Land");
		List<List<Coord>> LandRegions = GetRegions (1);
		
		int LandSize = 3;
		foreach (List<Coord> region in LandRegions) {
			if (region.Count < LandSize) {
				foreach (Coord voxel in region) {
					Map[voxel.posX, voxel.posY] = 0;
				}
			}
		}
		Profiler.EndSample ();
	}

	private void ProcessBridges () {
		Profiler.BeginSample ("Process Bridges");
		List<List<Coord>> Regions = GetRegions (0);
		List<Island> Islands = new List<Island> ();

		foreach (List<Coord> Region in Regions) {
			Islands.Add(new Island(Region, Map));
		}

		Islands.Sort ();
		Islands [0].isMain = true;
		Islands [0].isConnectedToMain = true;
		ConnectIslands (Islands);
		Profiler.EndSample ();
	}

	private void ConnectIslands (List<Island> Islands, bool ForceConnectMain = false) {
		Profiler.BeginSample ("ConnectIslands");
		bool FoundConnection = false;
		List<Island> IslandsA = new List<Island> ();
		List<Island> IslandsB = new List<Island> ();

		int nearest = Mathf.RoundToInt(Mathf.Infinity);
		Coord NearestVoxelA = new Coord();
		Coord NearestVoxelB = new Coord();
		Island NearestIslandA = new Island();
		Island NearestIslandB = new Island();

		if (ForceConnectMain) {
			foreach (Island island in Islands) {
				if (island.isConnectedToMain) {
					IslandsB.Add (island);
				} else {
					IslandsA.Add (island);
				}
			}
		} else {
			IslandsA = Islands;
			IslandsB = Islands;
		}

		foreach (Island IslandA in IslandsA) {
			if (!ForceConnectMain) { 
				FoundConnection = false; 

				if (IslandA.connectedIslands.Count > 0) {
					continue;
				}
			}
			
			foreach (Island IslandB in IslandsB) {
				if (IslandA == IslandB || IslandA.IsConnected(IslandB)) {
					continue;
				}
				
				for (int VoxelIndexA=0; VoxelIndexA < IslandA.EdgeVoxels.Count; VoxelIndexA++) {
					for (int VoxelIndexB=0; VoxelIndexB < IslandB.EdgeVoxels.Count; VoxelIndexB++) {
						Coord VoxelA = IslandA.EdgeVoxels[VoxelIndexA];
						Coord VoxelB = IslandB.EdgeVoxels[VoxelIndexB];
						int DistanceCalculated = Mathf.RoundToInt(Mathf.Pow(VoxelA.posX - VoxelB.posX, 2) + Mathf.Pow(VoxelA.posY - VoxelB.posY, 2));
						
						if (DistanceCalculated < nearest || !FoundConnection) {
							FoundConnection = true;
							
							nearest = DistanceCalculated;
							NearestVoxelA = VoxelA;
							NearestVoxelB = VoxelB;
							
							NearestIslandA = IslandA;
							NearestIslandB = IslandB;
						}
					}
				}
			}
			if (FoundConnection && !ForceConnectMain) {
				Bridge(NearestIslandA, NearestIslandB, NearestVoxelA, NearestVoxelB);
			}
		}

		if (FoundConnection && ForceConnectMain) {
			Bridge(NearestIslandA, NearestIslandB, NearestVoxelA, NearestVoxelB);
			ConnectIslands(Islands, true);
		}

		if (!ForceConnectMain) {
			ConnectIslands(Islands, true);
		}
		Profiler.EndSample ();
	}

	private void Bridge (Island IslandA, Island IslandB, Coord VoxelA, Coord VoxelB) {
		Island.ConnectIslands (IslandA, IslandB);

		List<Coord> line = GetLine (VoxelA, VoxelB);

		foreach (Coord c in line) {
			DrawCircle(c, 2);
		}
	}

	private void DrawCircle (Coord c, int r) {
		for (int x=-r; x <= r; x++) {
			for (int y=-r; y <= r; y++) {
				if (x*x + y*y <= r*r) {
					int GridX = c.posX + x;
					int GridY = c.posY + y;
					if (IsInsideBounds(GridX, GridY)) {
						Map[GridX, GridY] = 0;
					}
				}
			}
		}
	}

	private List<Coord> GetLine (Coord from, Coord to) {
		List<Coord> line = new List<Coord> ();
		bool inverted = false;

		int x = from.posX;
		int y = from.posY;

		int dx = to.posX - from.posX;
		int dy = to.posY - from.posY;

		int longest = Mathf.Abs (dx);
		int shortest = Mathf.Abs (dy);

		int HorizontalStep;
		int VerticleStep;

		if (longest < shortest) {
			inverted = true;
			longest = Mathf.Abs(dy);
			shortest = Mathf.Abs (dx);
			VerticleStep = Mathf.RoundToInt (Mathf.Sign (dx));
			HorizontalStep = Mathf.RoundToInt (Mathf.Sign (dy));
		} else {
			HorizontalStep = Mathf.RoundToInt (Mathf.Sign (dx));
			VerticleStep = Mathf.RoundToInt (Mathf.Sign (dy));
		}

		int GradientAccumulation = longest / 2;
		for (int i=0; i < longest; i++) {
			line.Add(new Coord(x,y));

			if (inverted) {
				y += HorizontalStep;
			} else {
				x += HorizontalStep;
			}

			GradientAccumulation += shortest;

			if (GradientAccumulation >= longest) {
				if (inverted) {
					x += VerticleStep;
				} else {
					y += VerticleStep;
				}
				GradientAccumulation -= longest;
			}
		}
		return line;
	}
}
