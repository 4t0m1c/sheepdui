﻿using UnityEngine;
using System.Collections;

public class Sheep : MonoBehaviour {
	// Global Public
	public float moveSpeed = 1f;
	public float hunger = 100f;
	public float waitTime = 0.5f;
	public float DisplayTime = 3f;
	public GameObject PsysPrefab;
	public GameObject SplatPrefab;
	public GameObject StatDisplayPrefab;

	// Global Private
	private Transform Manager;
	private GameObject Ground;
	private GameObject Psys;

	private Vector3 GuidePosition = Vector3.zero;
	private bool Guided = false;
	private bool needCommand;
	private bool falling = false;

	private Vector3 goal = Vector3.zero;
	private Vector3 startPos = Vector3.zero;
	private float lerpTime = 0f;
	private float rate;
	private bool waiting = false;
	private bool moving = true;
	private float waitCooldown;
	private Ground groundBelow;
	private bool FoundFood = false;
	private bool TestFood = false;
	private bool Fin = false;

	private float DisplayCountdown;
	private GameObject DisplayGUI;
	private Transform sheepSprite;
	private Animator sAnimator;
	
	/* ########################################################################################################################### */
	// Finds nearest chunk center to position
	private Transform FindChunk (Vector3 pos) {
		Transform targetChunk = null;
		float nearest = Mathf.Infinity;
		for (int i = 0; i < Manager.childCount; i++) {
			Transform o = Manager.GetChild(i);
			float distance = Vector3.Distance(o.position, pos);
			if (distance < nearest) {
				nearest = distance;
				targetChunk = o;
			}
		}
		return targetChunk;
	}

	// Finds nearest Voxel center to position
	private Transform FindVoxel (Vector3 pos, int type) {
		Transform chunk = FindChunk (pos).GetChild (type);
		Transform targetVoxel = null;
		float nearest = Mathf.Infinity;
		for (int i = 0; i < chunk.childCount; i++) {
			Transform o = chunk.GetChild(i);
			float distance = Vector3.Distance(o.position, pos);
			if (distance < nearest) {
				nearest = distance;
				targetVoxel = o;
			}
		}
		return targetVoxel;
	}

	// Use this for initialization
	void Start () {
		Manager = GameObject.Find ("Chunk Manager").transform;
		Manager.GetComponent<SheepCount> ().addSheep ();
		sheepSprite = transform.GetChild (0);
		sAnimator = sheepSprite.GetComponent<Animator> ();
		rate = 1.0f / moveSpeed; // 1.0f / time to complete
		needCommand = true;
	}
	

	// Update is called once per frame
	void Update () {
		if (!Fin) {
			if (DisplayCountdown > 0) {
				DisplayCountdown -= Time.deltaTime;

				if (DisplayCountdown <= 0) {
					Destroy (DisplayGUI);
				}
			}

			hunger -= 1 * Time.deltaTime;
			hunger = Mathf.Clamp (hunger, 0f, 100f);

			TestTrap ();
			TestFall ();

			TestGround (transform.position, true);
			if (falling) {
				Fall ();
			} else if (!Guided) {
				if (FoundFood && !moving) {
					if (waitCooldown <= 0) {
						//if (hunger <= 90) {
						Eat ();

						//}
						waiting = false;
					} else {
						waitCooldown -= Time.deltaTime;
					}
				} else {
					moving = true;
					Move ();
				}
			} else {
				moving = true;
				Move ();
			}
		} else {
			waitCooldown -= Time.deltaTime;

			if (waitCooldown <= 0) {
				Destroy(gameObject);
			}
		}
	}

	public void Die (bool leaveMark) {
		if (Psys != null) {
			Destroy(Psys);
		}

		if (leaveMark) {
			GameObject s = Instantiate(SplatPrefab, transform.position, transform.rotation) as GameObject;
			s.transform.position = this.transform.position;
		}
		Manager.GetComponent<SheepCount> ().removeSheep ();
		Destroy(this.gameObject);
	}

	public void Remove () {
		if (Psys != null) {
			Destroy(Psys);
		}
		Destroy(this.gameObject);
	}

	public void Eat () {
		if (FoundFood) {

		}
		groundBelow.setEaten();
		hunger += 10;
		hunger = Mathf.Clamp(hunger, 0f, 100f);
	}

	public void Fall () {
		transform.localScale -= Vector3.one * Time.deltaTime;
		if (transform.localScale.x <= 0) {
			Die (false);
		}
	}

	public void Move () {
		if (needCommand) {
			if (Guided) {
				goal = MoveGuided ();
				sAnimator.SetFloat("X", goal.x);
				sAnimator.SetFloat("Y", goal.z);
				if (goal.x > 0f || goal.z < 0f) {
					sheepSprite.localScale = new Vector3(-0.5f, sheepSprite.localScale.y, sheepSprite.localScale.z);
				} else if (goal.x < 0f || goal.z > 0f) {
					sheepSprite.localScale = new Vector3(0.5f, sheepSprite.localScale.y, sheepSprite.localScale.z);
				}
				goal += transform.position;
			} else {
				goal = MoveRandomly ();
				sAnimator.SetFloat("X", goal.x);
				sAnimator.SetFloat("Y", goal.z);
				if (goal.x > 0f || goal.z < 0f) {
					sheepSprite.localScale = new Vector3(-0.5f, sheepSprite.localScale.y, sheepSprite.localScale.z);
				} else if (goal.x < 0f  || goal.z > 0f) {
					sheepSprite.localScale = new Vector3(0.5f, sheepSprite.localScale.y, sheepSprite.localScale.z);
				}
				goal += transform.position;
			}
			startPos = transform.position;
			lerpTime = 0f;
			needCommand = false;
		} else {
			sAnimator.SetBool("Moving", true);
			if (lerpTime < 1.0f) {
				lerpTime += Time.deltaTime * rate;
				lerpTime = Mathf.Clamp (lerpTime, 0f, 1f);
				transform.position = Vector3.Lerp (startPos, goal, lerpTime);
			} else {
				sAnimator.SetBool("Moving", false);
				needCommand = true;
				if (Guided) {
					transform.position = new Vector3 (Mathf.Round (transform.position.x), transform.position.y, Mathf.Round (transform.position.z));
					if (transform.position.x == GuidePosition.x && transform.position.z == GuidePosition.z) {
						DestroyImmediate (Psys);
						Psys = null;
						Guided = false;
						moving = false;
					}
				} else {
					moving = false;
				}
			}
		}
	}

	public void TestTrap () {
		Transform nearestTrap = FindVoxel(transform.position, 1);
		if (nearestTrap != null) {
			if (Vector3.Distance(nearestTrap.position, transform.position) <= 0.5f) {
				Trap t = nearestTrap.GetComponent <Trap>();
				if (t.getTriggered() == false) {
					t.TriggerTrap();
				}
			}
		}
	}

	public void TestFall () {
		if (Vector3.Distance(FindVoxel(transform.position, 3).position, transform.position) <= 0.5f) {
			falling = true;
		}
	}

	public void TestGround (Vector3 pos, bool setGround) {
		Transform voxel = FindVoxel (pos, 2);
		if (voxel != null) {
			transform.position = new Vector3 (transform.position.x, voxel.position.y, transform.position.z);
			Ground ground = voxel.GetComponent<Ground> ();
			if (ground.getEaten () == false) {
				if (setGround) {
					groundBelow = ground;
					FoundFood = true;
					if (!waiting & !moving) {
						waiting = true;
						waitCooldown = waitTime;
					}
				} else {
					TestFood = true;
				}
			} else {
				if (setGround) {
					groundBelow = ground;
					FoundFood = false;
				} else {
					TestFood = false;
				}
			}
		} else {
			TestFood = true;
		}
	}

	private Vector3 GetRandomDirection () {
		Random.seed = Mathf.RoundToInt(Time.time + Random.Range(1, 10000) + Time.deltaTime);
		
		float min = -1f;
		float max = 1f;
		
		float x = Random.Range (min, max);
		float z = Random.Range (min, max);
		
		x = (x > 0) ? Mathf.Ceil (x) : Mathf.Floor (x);
		z = (z > 0) ? Mathf.Ceil (z) : Mathf.Floor (z);

		return new Vector3 (x, 0, z);
	}
	
	// Moves in a random direction
	private Vector3 MoveRandomly() {
		bool foundDirection = false;
		Vector3 goal = Vector3.zero;
		int tries = 0;
		while (foundDirection == false) {
			tries += 1;
			goal = GetRandomDirection ();
			if (TestPosition(goal + transform.position)) {
				TestGround (goal + transform.position, false);
				if (TestFood) {
					foundDirection = true;
					break;
				}
			}
			if (tries > 3) {
				if (TestPosition(goal + transform.position)) {
					foundDirection = true;
					break;
				}
			}
		}
		return goal;
	}

	// Moves to the player's guided position
	private Vector3 MoveGuided() {
		Vector3 goalDirection = GuidePosition - transform.position;
		goalDirection.y = 0;
		goalDirection.Normalize ();

		float x = goalDirection.x;
		float z = goalDirection.z;

		x = (x > 0) ? Mathf.Ceil (x) : Mathf.Floor (x);
		z = (z > 0) ? Mathf.Ceil (z) : Mathf.Floor (z);
		
		Vector3 goal = new Vector3 (x, 0, z);
		if (TestPosition (goal + transform.position)) {
			return goal;
		} else {
			return Vector3.zero;
		}
	}

	// Sets sheep to guided role through BasicMove
	public void setGuided (Vector3 position) {
		if (Psys == null) {
			Psys = GameObject.Instantiate (PsysPrefab, position, Quaternion.Euler (0, 0, 0)) as GameObject;
		} else {
			Psys.transform.position = position;
		}
		GuidePosition = position;
		GuidePosition.y = 0;
		Guided = true;
	}

	// Switches on display Items
	public void DisplayStats () {
		if (DisplayCountdown <= 0) {
			DisplayGUI = Instantiate (StatDisplayPrefab, Vector3.zero, Quaternion.identity) as GameObject;
			DisplayGUI.transform.parent = transform;
			DisplayGUI.transform.localPosition = Vector3.zero;
			
			DisplayCountdown = DisplayTime;
		}
	}
	
	// Test if path is blocked
	public bool TestPosition (Vector3 pos) {
		bool blocked = true;
		blocked = (Vector3.Distance (FindVoxel (pos, 0).position, pos) <= 1f) ? false : true;
		if (blocked == true) {
			GameObject[] allSheep = GameObject.FindGameObjectsWithTag("Sheep");
			for (int i = 0; i<allSheep.Length; i++) {
				if (allSheep[i] != this.gameObject){
					blocked = (Vector3.Distance (allSheep[i].transform.position, pos) <= 1f) ? false : true;
					if (!blocked) {
						break;
					}
				}
			}
		}

		return blocked;
	}

	public void Finished () {
		Fin = true;
		waitCooldown = waitTime;
	}
}