﻿using UnityEngine;
using System.Collections;

public class Ground : MonoBehaviour {
	public Sprite[] spriteImage;
	public bool Eaten = false;
	private int RandomNumber = 0;

	private SpriteRenderer grassTile;

	void Start () {
		grassTile = transform.GetChild (0).GetComponent<SpriteRenderer> ();
		RandomNumber = Random.Range (0,3);
		grassTile.sprite = spriteImage[RandomNumber + 4];
	}


	public bool getEaten() {
		return Eaten;
	}

	public void setEaten() {
		Eaten = true;
		grassTile.sprite = spriteImage[RandomNumber];
	}

	public void Reset () {
		Eaten = false;
		RandomNumber = Random.Range (0,3);
		grassTile.sprite = spriteImage[RandomNumber + 4];
	}
}
 