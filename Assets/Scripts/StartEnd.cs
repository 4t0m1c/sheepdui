﻿using UnityEngine;
using System.Collections;

/*
 * Script Role:
 *	Provide 3 States: 
 *	 1) Position the camera at the start position and spawn in sheep 
 *	 2) Monitor that the sheep have not all died 
 *	 3) Clean up after all the sheep are dead or finish is reached
 */

public class StartEnd : MonoBehaviour {
	public GameObject SheepPrefab;
	public SheepCount sCount;
	public Level_Load LvlLoad;
	public UI_Controller uiCtrl;

	public Transform[] StartZone;
	public Transform[] EndZone;
	public int MinSheep;

	private int SheepCount;
	public int FinishedSheep = 0;

	private bool Started = false;

	public void StartMap (int minSheep) {
		MinSheep = minSheep;
		SheepCount = sCount.currentsheepcount;
		FinishedSheep = 0;

		// Set the camera to the middle of the StartZone
		Transform centerPos = StartZone[StartZone.Length/2];
		Transform cam = GameObject.Find ("Camera Mover").transform;
		cam.transform.position = new Vector3 (centerPos.position.x, 2, centerPos.position.z);
		GameObject.Instantiate (SheepPrefab, centerPos.position, Quaternion.Euler (0, 0, 0));

		Started = true;
	}

	public void EndMap (bool win) {
		Started = false;
		GameObject[] Sheepz = GameObject.FindGameObjectsWithTag("Sheep");
		
		foreach (GameObject o in Sheepz) {
			o.GetComponent<Sheep> ().Remove();
		}

		if (win) {
			sCount.FlockCount += 1;
			PlayerPrefs.SetInt ("FlockCount",sCount.FlockCount);
			LvlLoad.CompleteLevel(LvlLoad.LastLevel, 1);
		}
		uiCtrl.Defaults (null);
	}

	public bool TestForSheep () {
		if (SheepCount > 0) {
			return true;
		} else {
			return true;
		}
	}

	public bool TestForEnd () {
		GameObject[] allSheep = GameObject.FindGameObjectsWithTag ("Sheep"); 

		foreach (GameObject sheep in allSheep) {
			Transform sheepT = sheep.transform;
			foreach (Transform voxel in EndZone) {
				if (Vector3.Distance(voxel.position, sheepT.position) <= 0.5f) {
					FinishedSheep += 1;
					sheepT.GetComponent<Sheep> ().Finished ();
					sheepT.GetChild(0).GetComponent<SpriteRenderer> ().material.color = Color.blue;
					break;
				}
			}
		}

		if (FinishedSheep >= MinSheep) {
			return true;
		} else {
			return false;
		}
	}

	void Update () {
		if (Started) {
			if (TestForSheep()) {
				if (TestForEnd()) {
					EndMap(true);
				}
			} else {
				EndMap(false);
			}
		}
	}
}
