﻿using UnityEngine;
using System.Collections;

public class SpawnClouds : MonoBehaviour {
	public int Width = 10;
	public int Height = 10;
	public GameObject[] clouds; 

	private Transform LastCloud;
	private Transform CurrentCloud;
	//private float StartWait = 10;

	public void Start () {
		for (int x=0; x<Width; x++) {
			SpawnRow(x);
		}


	}

	public void SpawnRow (int PosX) {
		GameObject cloudRow = new GameObject();
		cloudRow.transform.parent = this.transform;
		cloudRow.name = "CloudRow";
		cloudRow.transform.localPosition = new Vector3((PosX - (Width / 2)) * 10, -1, 0);

		for (int y=0; y<Height; y++) {
			int randomVal = Mathf.RoundToInt(Random.Range(0, clouds.Length));
			GameObject c = Instantiate(clouds[randomVal].gameObject, Vector3.zero, Quaternion.identity) as GameObject;
			c.transform.parent = cloudRow.transform;
			c.transform.localPosition = new Vector3(0, 0, (y - (Height / 2)) * 10);
		}

		cloudRow.AddComponent<CloudMove>();

		//LastCloud = CurrentCloud;
		//CurrentCloud = cloudRow.transform;
	}
}
