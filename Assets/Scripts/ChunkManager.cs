using UnityEngine;
using System.Collections;

public class ChunkManager : MonoBehaviour {
	public GameObject[] TerrainPrefabs = new GameObject[1];
	public int[] TerrainSpawnChance = new int[1];
	public int TerrainSpawnDensity = 100;

	public GameObject[] TrapPrefabs = new GameObject[1];
	public int[] TrapSpawnChance = new int[1];
	public int TrapSpawnDensity = 30;

	public int DrawDistance = 5;
	public float Size = 16f;
	public int myResolution = 32;
	public float HeightMultiplier = 1f;
	public float PerlinScale = 5f;
	public int Seed = 1234;

	public bool LoadAll = false;

	public GameObject Target;
	private GameObject[] RandomTrapsList;

	// Use this for initialization
	void Start () {
		Target = GameObject.Find ("Camera Mover");
		myResolution = Mathf.Clamp (myResolution, 8, 64);
	}
	
	// Update is called once per frame
	void Update () {
		#if UNITY_EDITOR
			if (!LoadAll) {
				CleanupChunks ();
				AddChunks ();
			}
		#else
			CleanupChunks ();
			AddChunks ();
		#endif
	}

	public void ResetWorld() {
		transform.GetComponent<ChunkCreator> ().ResetWorld();
	}

	private void CleanupChunks () {
		GameObject[] Sheepz = GameObject.FindGameObjectsWithTag ("Sheep");

		for (int i = 0; i < transform.childCount; i++) {
			Transform t = transform.GetChild(i);
			bool shouldDisable = true;
			if (Vector3.Distance(t.position, Target.transform.position) < DrawDistance) {
				shouldDisable = false;
			} else {
				for (int s = 0; s < Sheepz.Length; s++) {
					if (Vector3.Distance(Sheepz[s].transform.position, t.position) < DrawDistance) {
						shouldDisable = false;
						break;
					}
				}
			}
			if (shouldDisable && t.gameObject.activeSelf == true) {
				t.gameObject.SetActive(false);
			}
		}
	}
	
	public void AddChunks () {
		GameObject[] Sheepz = GameObject.FindGameObjectsWithTag ("Sheep");
		
		for (int i = 0; i < transform.childCount; i++) {
			Transform t = transform.GetChild(i);
			bool shouldEnable = false;
			if (Vector3.Distance(t.position, Target.transform.position) < DrawDistance) {
				shouldEnable = true;
			} else {
				for (int s = 0; s < Sheepz.Length; s++) {
					if (Vector3.Distance(Sheepz[s].transform.position, t.position) < DrawDistance) {
						shouldEnable = true;
						break;
					}
				}
			}
			
			if (shouldEnable && t.gameObject.activeSelf == false) {
				t.gameObject.SetActive(true);
			}
		}
	}

	public void LoadRandomTrapList () {
		int[] counts = new int[TrapSpawnChance.Length];
		int Range = 0;
		int Pointer = 0;


		for (int i = 0; i< TrapSpawnChance.Length; i++) {
			Range += TrapSpawnChance[i];
			counts[i] = TrapSpawnChance[i];
		}
		int Countdown = Range;

		GameObject[] result = new GameObject[Range];

		while (true) {
			for (int i = 0; i < counts.Length; i++) {
				if (counts[i] != 0) {
					result[Pointer] = TrapPrefabs[i];
					counts[i] -= 1;
					Countdown -= 1;
					Pointer += 1;
				}
			}
			if (Countdown <= 0) {
				break;
			}
		}

		int tmp = 0;
		for (int i = 0; i< counts.Length; i++) {
			tmp += counts[i];
		}

		RandomTrapsList = result;
	}
	
	public GameObject RandomTrapObject() {
		int RandomValue = Random.Range (0, RandomTrapsList.Length - 1); 

		return RandomTrapsList [RandomValue];
	}

	public GameObject RandomTerrainObject() {
		int Range = 0;
		for (int i = 0; i< TerrainSpawnChance.Length; i++) {
			Range += TerrainSpawnChance[i];
		}
		
		int RandomValue = Random.Range (0, Range); 
		int currentNearestValue = TerrainSpawnChance [0];
		GameObject currentNearestObject = TerrainPrefabs [0];
		int currentDifference = Mathf.Abs (currentNearestValue - RandomValue);
		
		for (int i = 0; i< TerrainSpawnChance.Length; i++) {
			int differene = Mathf.Abs(TerrainSpawnChance[i] - RandomValue);
			
			if (differene < currentDifference) {
				currentDifference = differene;
				currentNearestValue = TerrainSpawnChance[i];
				currentNearestObject = TerrainPrefabs[i];
			}
		}
		return currentNearestObject;
	}

	public Transform FindChunk (Vector3 pos) {
		Transform targetChunk = null;
		float nearest = Mathf.Infinity;
		for (int i = 0; i < transform.childCount; i++) {
			Transform o = transform.GetChild(i);
			float distance = Vector3.Distance(o.position, pos);
			if (distance < nearest) {
				nearest = distance;
				targetChunk = o;
			}
		}
		return targetChunk;
	}

}
