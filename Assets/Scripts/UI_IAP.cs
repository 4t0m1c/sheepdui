﻿using UnityEngine;
using System.Collections;

public class UI_IAP : MonoBehaviour {

	public UI_Controller _UI_Controller;
	public RectTransform Tier;
	public GameObject Tier1;
	public GameObject Tier2;
	public GameObject Tier3;
	public GameObject Tier4;
	public GameObject LeftArrow;
	public GameObject RightArrow;
	public RectTransform ActiveTierGO;
	public bool LeftSwipe = false;
	public bool RightSwipe = false;
	public int SwipeAmount;

	// Use this for initialization
	void Start () {
		Tier = this.GetComponent<RectTransform>();
		StartCoroutine (Wiggle ());
	}

	void OnEnable(){
		Debug.Log ("Enabled");
		StopAllCoroutines();
		StartCoroutine (Wiggle ());
	}

	public void ActiveTier(GameObject Tier){
		ActiveTierGO = Tier.transform.GetChild(0).GetComponent<RectTransform>();
	}

	public void SwipeRight(){
		switch (ActiveTierGO.transform.name){
		case "Tier_1":
		{
			//nothing to do
			break;
		}
		case "Tier_2":
		{
			_UI_Controller.HitButton();
			Tier1.SetActive(true);
			iTween.StopByName("LeftArrow");
			LeftArrow.transform.localScale = new Vector3 (1,1,1);
			LeftArrow.SetActive(false);
			Tier2.transform.GetChild(0).GetComponent<RectTransform>().localPosition = new Vector3(0,0,0);
			Tier2.SetActive(false);
			break;
		}
		case "Tier_3":
		{
			_UI_Controller.HitButton();
			Tier2.SetActive(true);
			Tier3.transform.GetChild(0).GetComponent<RectTransform>().localPosition = new Vector3(0,0,0);
			Tier3.SetActive(false);
			break;
		}
		case "Tier_4":
		{
			_UI_Controller.HitButton();
			Tier3.SetActive(true);
			RightArrow.SetActive(true);
			Tier4.transform.GetChild(0).GetComponent<RectTransform>().localPosition = new Vector3(0,0,0);
			Tier4.SetActive(false);
			break;
		}
		}
	}
		
	public void SwipeLeft(){
		switch (ActiveTierGO.transform.name){
		case "Tier_1":
		{
			_UI_Controller.HitButton();
			Tier2.SetActive(true);
			LeftArrow.SetActive(true);
			Tier1.transform.GetChild(0).GetComponent<RectTransform>().localPosition = new Vector3(0,0,0);
			Tier1.SetActive(false);
			break;
		}
		case "Tier_2":
		{
			_UI_Controller.HitButton();
			Tier3.SetActive(true);
			Tier2.transform.GetChild(0).GetComponent<RectTransform>().localPosition = new Vector3(0,0,0);
			Tier2.SetActive(false);
			break;
		}
		case "Tier_3":
		{
			_UI_Controller.HitButton();
			Tier4.SetActive(true);
			iTween.StopByName("RightArrow");
			RightArrow.transform.localScale = new Vector3 (1,1,1);
			RightArrow.SetActive(false);
			Tier3.transform.GetChild(0).GetComponent<RectTransform>().localPosition = new Vector3(0,0,0);
			Tier3.SetActive(false);
			break;
		}
		case "Tier_4":
		{
			//nothing to do
			break;
		}
		}
	}
			
			// Update is called once per frame
	void Update () {
		if (this.transform.parent.gameObject.activeInHierarchy == true){
			foreach (Touch touch in Input.touches) {
				switch (touch.phase) {
				case TouchPhase.Began:
				{
					//not needed
					break;
				}
				case TouchPhase.Moved:
				{
					if (ActiveTierGO.localPosition.x > SwipeAmount){RightSwipe = true;}
					if (ActiveTierGO.localPosition.x < SwipeAmount && ActiveTierGO.localPosition.x > -SwipeAmount){RightSwipe = false; LeftSwipe = false;}
					if (ActiveTierGO.localPosition.x < -SwipeAmount){LeftSwipe = true;}
					break;
				}
				case TouchPhase.Ended:
				{
					if (RightSwipe == true){ //right
							SwipeRight ();
						}
					
					if (LeftSwipe == true){ //left
							SwipeLeft();
						}
					RightSwipe = false;
					LeftSwipe = false;
					break;
				}
				}
			}
		}
	}

	public IEnumerator Wiggle(){
		while (this.enabled == true) {
			Debug.Log ("Wiggle Started");
			yield return new WaitForSeconds (Random.Range (3, 7));
			if (RightArrow.activeSelf == true) {
				iTween.PunchRotation (RightArrow, new Vector3 (0, 0, -30), 1f);
				yield return new WaitForSeconds (1f);
				Debug.Log ("Wiggle Right");
			} else {
				iTween.PunchRotation (LeftArrow, new Vector3 (0, 0, 30), 1f);
				yield return new WaitForSeconds (1f);
				Debug.Log ("Wiggle Left");
			}
		} 
		StopAllCoroutines();
	}

	public void ScaleRightButtonDown(){
		iTween.ScaleTo(RightArrow, iTween.Hash("name","RightArrow","scale",new Vector3(1.3f,1.3f,1f),"time",0.1f,"easetype","easeInOutQuart"));
	}
	
	public void ScaleLeftButtonDown(){
		iTween.ScaleTo(LeftArrow, iTween.Hash("name","LeftArrow","scale",new Vector3(1.3f,1.3f,1f),"time",0.1f,"easetype","easeInOutQuart"));
	}
	
	public void ScaleRightButtonUp(){
		iTween.ScaleTo(RightArrow, iTween.Hash("name","RightArrow","scale",new Vector3(1f,1f,1f),"time",0.2f,"easetype","easeInOutQuart"));
	}
	
	public void ScaleLeftButtonUp(){
		iTween.ScaleTo(LeftArrow, iTween.Hash("name","LeftArrow","scale",new Vector3(1f,1f,1f),"time",0.2f,"easetype","easeInOutQuart"));
	}
}
