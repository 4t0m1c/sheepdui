﻿using UnityEngine;
using System.Collections;

public class UI_Drag : MonoBehaviour {
	
	public Vector3 startPos;
	public Vector3 touchWorldPos;
	public Vector3 offset;
	public float distance;
	public Vector3 endPos;
	public Transform target;
	Camera UI_Cam;
	public bool canDrag = false;
	public UI_Controller _UI_Controller;
	public bool change = false;
	public bool moveTouch = false;
	public bool adjustTouch = false;
	Sprite[] textures;
	float volsound;
	float volmusic;
	
	void Start () {
		UI_Cam = GameObject.Find("UI_Camera").transform.GetComponent<Camera>();
		_UI_Controller = GameObject.Find("UI_Controller").GetComponent<UI_Controller>();
		textures = Resources.LoadAll<Sprite>("sprites/menu_loadbars_sprite");
		volsound = PlayerPrefs.GetFloat("Volume_Sound");
		volmusic = PlayerPrefs.GetFloat("Volume_Music");
		for (int i = 0; i<3; i++){
			if (this.transform.GetComponent<SpriteRenderer>().sprite.name == "sign_"+(i+1)){
				if (transform.GetChild(0).GetComponent<TextMesh>().text == "SOUND VOLUME"){
					Debug.Log ("Sign_"+(i+1)+": "+volsound);
					this.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = textures[(int)volsound+((i)*13)];
				}
				if (transform.GetChild(0).GetComponent<TextMesh>().text == "MUSIC VOLUME"){
					Debug.Log ("Sign_"+(i+1)+": "+volmusic);
					this.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = textures[(int)volmusic+((i)*13)];
				}
			}
		}
	}
	
	void Update () {
		foreach (Touch touch in Input.touches) {
			switch (touch.phase) {
			case TouchPhase.Began:
			{
				Debug.Log("Touch Begin");
				int mask = (1<<10);
				RaycastHit2D hit = Physics2D.Raycast (UI_Cam.ScreenToWorldPoint(touch.position),Vector2.zero,Mathf.Infinity,mask);
				target = hit.transform;
				//Debug.Log("Raycast Hit: "+hit.collider.gameObject);
				if (hit.transform == this.transform){
					startPos = UI_Cam.ScreenToWorldPoint(touch.position);
					startPos = new Vector3 (startPos.x, startPos.y, 0);
					offset = startPos - new Vector3(this.transform.position.x,this.transform.position.y,0);
					Debug.Log ("StartPos: "+startPos+", Offset: "+offset);
					if (hit.transform.childCount == 1){
						Debug.Log ("MOVE");
						moveTouch = true;
					} else if (hit.transform.childCount == 2){
						Debug.Log ("ADJUST");
						canDrag = true;
						adjustTouch = true;
						startPos = new Vector3(startPos.x, 0,0);
						offset = new Vector3 (offset.x, 0, 0);
						startPos = startPos-offset;
					}
				}
				break;
			}
			case TouchPhase.Moved:
			{
				canDrag = true;
				if (moveTouch == true){
					if (target == this.transform){
						touchWorldPos = UI_Cam.ScreenToWorldPoint (new Vector3 (touch.position.x,touch.position.y));
						distance = Vector3.Distance(touchWorldPos,startPos);
						if (distance > 0.4){
							transform.GetComponent<Rigidbody2D>().isKinematic = true;
							Debug.Log("NewPos: "+(touchWorldPos-offset));
							endPos = touchWorldPos-offset;
							this.transform.position = new Vector3 (endPos.x, endPos.y,transform.parent.parent.position.z);
							//this.transform.Translate(endPos-startPos*Time.deltaTime);
						}
					}
				}
				if (adjustTouch == true){
					touchWorldPos = UI_Cam.ScreenToWorldPoint (new Vector3 (touch.position.x,touch.position.y));
					touchWorldPos = new Vector3 (touchWorldPos.x, 0 ,0);
					distance = ((touchWorldPos.x-startPos.x)+3)/6;
					ChangeVolume();
				}
				break;
			}
			case TouchPhase.Ended:
			{
				moveTouch = false;
				adjustTouch = false;
				distance = 0;
				transform.GetComponent<Rigidbody2D>().isKinematic = false;
				if (target == this.transform){
					if (canDrag == false){
						StartCoroutine(Wobble());
					}
				}
				canDrag = false;
				break;
			}
			}
		}
	}
	
	IEnumerator Wobble(){
		iTween.MoveBy(transform.gameObject,iTween.Hash("amount",new Vector3(0.4f,0f,0f),"time",0.2f,"easetype","easeInOutQuart"));	
		yield return new WaitForSeconds(0.2f);
		switch (transform.GetChild(0).GetComponent<TextMesh>().text){
			//PAUSE
		case "RESUME":
		{
			_UI_Controller.Menu_Play.SetActive (true);
			_UI_Controller.HitButton();
			_UI_Controller.Menu_Pause.SetActive (false);
			break;
		}
		case "OPTIONS":
		{

			_UI_Controller.Settings(_UI_Controller.Menu_Pause); //UI_Controller.cs
			break;
		}
		case "EXIT":
		{
			_UI_Controller.HitButton();
			_UI_Controller.Menu_Pause.SetActive (false);
			_UI_Controller.Defaults(_UI_Controller.Menu_Pause); //UI_Controller.cs
			break;
		}
			//OPTIONS
		case "RESET":
		{
			GameObject.Find("GameOptions").GetComponent<Options>().ResetProgress();
			break;
		}
		case "RESTORE PURCHASES":
		{
			
			break;
		}
			//MAIN MENU
		case "PLAY":
		{
			_UI_Controller.HitButton();
			_UI_Controller.Level(_UI_Controller.Menu_Main);
			break;
		}
		case "STORE":
		{
			_UI_Controller.HitButton();
			_UI_Controller.Flock(_UI_Controller.Menu_Main);
			break;
		}
		case "ACHIEVEMENTS":
		{
			_UI_Controller.HitButton();
			_UI_Controller.Achievements(_UI_Controller.Menu_Main);
			break;
		}
		}
		
		target = null;
	}
	
	void ChangeVolume(){
		float factor = 0.0769230769230769f;
		SpriteRenderer loadingbar = this.transform.GetChild(1).GetComponent<SpriteRenderer>();
		string volType = "";
		float volume;
		if (transform.GetChild(0).GetComponent<TextMesh>().text == "SOUND VOLUME"){volType = "Volume_Sound";}
		if (transform.GetChild(0).GetComponent<TextMesh>().text == "MUSIC VOLUME"){volType = "Volume_Music";}
		
		switch (this.transform.GetComponent<SpriteRenderer>().sprite.name){
		case "sign_1":
		{
			for (int i = 0; i<13; i++){
				if ((distance > i*factor) && (distance < (i+1)*factor)){
					loadingbar.sprite = textures[i];
					volume=(((i)*factor)*13);
					Debug.Log ("Sign_1: "+volume);
					PlayerPrefs.SetFloat(volType,volume);//SPRITE INDEX
					volume=_UI_Controller.volumeConvert(volume);
					if (volume <= -(_UI_Controller.MinVol-1)){volume = -80;}
					if (volType == "Volume_Sound"){_UI_Controller.SetSoundVol(volume);}//SOUND
					if (volType == "Volume_Music"){_UI_Controller.SetMusicVol(volume);}//MUSIC
				}
			}
			break;
		}
		case "sign_2":
		{
			for (int i = 0; i<13; i++){
				if (distance > i*factor && distance < (i+1)*factor){
					loadingbar.sprite = textures[i+13];
					volume=(((i)*factor)*13);
					Debug.Log ("Sign_2: "+volume);
					PlayerPrefs.SetFloat(volType,volume);//SPRITE INDEX
					volume=_UI_Controller.volumeConvert(volume);
					if (volume <= -(_UI_Controller.MinVol-1)){volume = -80;}
					if (volType == "Volume_Sound"){_UI_Controller.SetSoundVol(volume);}//SOUND
					if (volType == "Volume_Music"){_UI_Controller.SetMusicVol(volume);}//MUSIC
				}
			}
			break;
		}
		case "sign_3":
		{
			for (int i = 0; i<13; i++){
				if (distance > i*factor && distance < (i+1)*factor){
					loadingbar.sprite = textures[i+26];
					volume=(((i)*factor)*13);
					Debug.Log ("Sign_3: "+volume);
					PlayerPrefs.SetFloat(volType,volume);//SPRITE INDEX
					volume=_UI_Controller.volumeConvert(volume);
					if (volume <= -(_UI_Controller.MinVol-1)){volume = -80;}
					if (volType == "Volume_Sound"){_UI_Controller.SetSoundVol(volume);}//SOUND
					if (volType == "Volume_Music"){_UI_Controller.SetMusicVol(volume);}//MUSIC
				}
			}
			break;
		}
		}
	}
}
