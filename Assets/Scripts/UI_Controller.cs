﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Audio;

public class UI_Controller : MonoBehaviour {
	
	public GameObject Menu_Main;
	public GameObject Menu_Play;
	public GameObject Menu_Pause;
	public GameObject Menu_Level;
	public GameObject Menu_Settings;
	public GameObject Menu_Flock;
	public GameObject Menu_Achievements;
	public GameObject Menu_NextLevel;
	public GameObject LevelGUIPrefab;
	public GameObject LastPage;

	public AudioMixer masterMixer;
	public float MinVol = 30;

	private GameObject go;
	private GameObject ui_ctrl;
	private bool CamMove = true;
	private float x;
	private float z;
	public Level_Load LoadLevel;
	public SheepCount sCount;

	void Awake () {
		//sCount = GameObject.Find ("Chunk Manager").GetComponent<SheepCount> ();
		//LoadLevel = GameObject.Find ("Chunk Manager").GetComponent<Level_Load> ();
		ui_ctrl = this.gameObject;
		go = Camera.main.transform.parent.transform.gameObject;
		CamMove = true;
	}

	void Start () {
		//Debug.Log ("--- BEGIN UI_Controller.cs ---");
		//SetMasterVol(-80);
		Defaults (null);
		newMove ();
		LoadLevels(LoadLevel.UnlockStats);
		//Debug.Log ("--- END UI_Controller.cs ---");

		//AUDIO
		float volsound = PlayerPrefs.GetFloat("Volume_Sound");
		float volmusic = PlayerPrefs.GetFloat("Volume_Music");
		volsound=volumeConvert(volsound);
		if (volsound <= -(MinVol-1)){volsound = -80;}
		SetSoundVol(volsound);
		volmusic=volumeConvert(volmusic);
		if (volmusic <= -(MinVol-1)){volmusic = -80;}
		SetMusicVol(volmusic);

	}

	public float volumeConvert(float volume){
		volume = (((volume/13)-1)*MinVol)+1;
		return volume;
	}

	public void HitButton(){
		GameObject.Find("UI_Sound").GetComponent<AudioSource>().Play();
	}

	void newMove(){
		//Debug.Log ("--- BEGIN newMove ---");
		x = Random.Range (-35, 35);
		z = Random.Range (-20, 20);
		var newPos = new Vector3 (x, 0, z);
		iTween.MoveTo (go, iTween.Hash ("name","camera","position", newPos, "speed", 0.3,"easetype","linear", "oncomplete", "newMove", "oncompletetarget", ui_ctrl));
		//Debug.Log ("--- END newMove ---");
	}

	public void Defaults(GameObject last){
		//Debug.Log ("--- BEGIN Main Menu ---");
		LastPage = last;
		Menu_Main.SetActive (true);
		//Menu_Pause.SetActive (false);
		Menu_Flock.SetActive (false);
		Menu_Play.SetActive (false);
		Menu_Settings.SetActive (false);
		Menu_Achievements.SetActive (false);
		Menu_Level.SetActive (false);
//		Camera.main.gameObject.GetComponent<UnityStandardAssets.ImageEffects.PostEffectsBase> ().enabled = true;
		if (CamMove == false) {
			newMove();
			CamMove = true;
		}
		//Debug.Log ("--- END Main Menu ---");
	}

	/*public void LevelCheck(int level){
		Debug.Log ("RanLevel");
		//Debug.Log ("--- BEGIN LevelCheck ---");
		//Debug.Log ("Level: " + level);
		bool access = LoadLevel.GetLevelAccess(level);
		Debug.Log ("Access: " + access);
		if (access == true){
			LoadLevel.LastLevel = level;
			LoadLevel.LoadLevel (level);
			Play();
		}
		//Debug.Log ("--- END LevelCheck ---");
	}*/

	public void Level(GameObject last){
		//Debug.Log ("--- BEGIN LevelSel ---");
		LoadLevels (LoadLevel.UnlockStats);
		Menu_Level.SetActive (true);
		Menu_Main.SetActive (false);
		LastPage = last;
		RectTransform ui_container = Menu_NextLevel.GetComponent<RectTransform> ();
		Text fCount = ui_container.transform.parent.transform.parent.transform.FindChild ("ui_sheep").transform.FindChild ("Text").GetComponent<Text> ();
		ui_container.sizeDelta = new Vector2((470 * (LoadLevel.UnlockStats.Count + 1)), ui_container.sizeDelta.y);
		fCount.text = sCount.FlockCount.ToString();
		HitButton();
		//Debug.Log ("fCOunt: " + fCount.text);

		//Debug.Log ("--- END LevelSel ---");
	}

	public void Play(GameObject last){
		//Debug.Log ("--- BEGIN Play ---");
		Menu_Play.SetActive (true);
		Menu_Level.SetActive (false);
		LastPage = last;
		iTween.StopByName ("camera");
		CamMove = false;
		Camera.main.transform.parent.transform.position = new Vector3 (0, 2, 0);
		HitButton();
//		Camera.main.gameObject.GetComponent<UnityStandardAssets.ImageEffects.PostEffectsBase> ().enabled = false;
		//Debug.Log ("--- END Play ---");
	}

	public void LoadLevels (List<LevelStats> LevelList) {
		// TODO This should go through the list of levels and update/add all the changed data for Stars and unlocks
		//Debug.Log ("--- BEGIN RefreshStats ---");
		RectTransform ui_container = Menu_NextLevel.GetComponent<RectTransform> ();

		for (var i=0;i<LevelList.Count;i++){

			//Cycle Current levels and refresh locks
			LoadLevel.LoadLevel(i);
			//Debug.Log ("LevelList Count: " + LevelList.Count);
			string lname = "level_"+i.ToString();
			GameObject n;
			try{
				n = ui_container.FindChild(lname).gameObject;
				//Debug.Log ("#UI Level was found");
				n.GetComponent<Level_Stats>().RefreshSheepLock();
				//Debug.Log ("--RefreshSheepLock()");
				n.GetComponent<Level_Stats>().CheckLock();
				//Debug.Log("--CheckLock()");
			}
			catch{
				// Create new GUI item
				//Debug.Log ("#UI Level was created");
				n = Instantiate (LevelGUIPrefab, Vector3.zero, Quaternion.identity) as GameObject;
				//Debug.Log ("--GO Instantiated");
				n.name = lname;
				//Debug.Log ("--Name Assigned");
				n.transform.SetParent(Menu_NextLevel.transform);
				//Debug.Log ("--Parent Set");
				n.transform.localScale = new Vector3 (1, 1, 1);
				//Debug.Log ("--Scale Set");
				n.GetComponent<Level_Stats>().NewLevel();
				//Debug.Log ("--Level_Stats NewLevel()");
			}
		}
		//Debug.Log ("--- END RefreshStats ---");
		
	}

	public void Settings(GameObject last){
		LastPage = last;
		Menu_Settings.SetActive (true);
		last.SetActive(false);
		HitButton();
	}

	public void Flock(GameObject last){
		LastPage = last;
		Menu_Flock.SetActive (true);
		last.SetActive(false);
	}

	public void Achievements(GameObject last){
		LastPage = last;
		Menu_Achievements.SetActive (true);
		last.SetActive(false);
	}

	public void Pause(GameObject last){
		LastPage = last;
		last.SetActive(false);
		Menu_Pause.SetActive(true);
		HitButton();
	}

	public void Last(GameObject last){
		LastPage.SetActive(true);
		LastPage = last;
		last.SetActive(false);
		HitButton();
	}

	public void SetMasterVol(float volume){
		masterMixer.SetFloat("MasterVol", volume);
	}

	public void SetMusicVol(float volume){
		masterMixer.SetFloat("MusicVol", volume);
	}

	public void SetSoundVol(float volume){
		masterMixer.SetFloat("SoundVol", volume);
	}

	//#if UNITY_EDITOR
	public void OnGUI () {
		if (Menu_Play.activeSelf) {
			if (GUI.Button (new Rect(0, 0, 50, 50), "END")) {
				StartEnd SE = sCount.transform.GetComponent<StartEnd> ();
				SE.EndMap(true);
			}
		}
	}
	//#endif
}
