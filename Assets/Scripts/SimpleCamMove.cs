using UnityEngine;

// This script will move the GameObject based on finger gestures
public class SimpleCamMove : MonoBehaviour
{/*
	//Move
	private RaycastHit hit;
	private Vector3 InitialHit;
	private Vector3 CurrentHit;
	private Vector3 DirectionHit;
	private bool CamActive;
	//Zoom
	private bool CamZoom;
	private Vector3 PrevA;
	private Vector3 PrevB;
	private float PrevDis;
	private float DeltaDis;
	private float PrevScale;
	private Vector3 PrevScaleV;
	private float NewScale;
	private Vector3 NewScaleV;
	private Vector3 NewA;
	private Vector3 NewB;
	private float CurrentAngle;
	private float MinAngle = 20;
	private float MaxAngle = 45;
	
	protected virtual void OnEnable()
	{
		// Hook into the OnFingerDown event
		Lean.LeanTouch.OnFingerDown += OnFingerDown;
		
		// Hook into the OnFingerUp event
		Lean.LeanTouch.OnFingerUp += OnFingerUp;
	}

	protected virtual void OnDisable()
	{
		// Unhook the OnFingerDown event
		Lean.LeanTouch.OnFingerDown += OnFingerDown;
		
		// Unhook the OnFingerUp event
		Lean.LeanTouch.OnFingerUp += OnFingerUp;
	}

	public void OnFingerDown(Lean.LeanFinger finger)
	{
		if (Lean.LeanTouch.Fingers.Count == 1) {
			var ray = finger.GetRay ();
			int layerMask = (1 << 8);
			CamActive = true;
			if (Physics.Raycast (ray, out hit, 1500, layerMask)) {
				InitialHit = hit.point;
			}
		}
	}

	protected virtual void LateUpdate()
	{
		if (Input.touchCount == 1) {
			if (CamActive) {
				var finger = Lean.LeanTouch.Fingers [Lean.LeanTouch.Fingers.Count - 1];
				var ray = finger.GetRay ();
				int layerMask = (1 << 8);
				if (Physics.Raycast (ray, out hit, 1500, layerMask)) {
					CurrentHit = hit.point;
					DirectionHit = (CurrentHit - InitialHit);
					this.transform.Translate (new Vector3 (-DirectionHit.x, 0, -DirectionHit.z));
				}
			}
		}
		if (Input.touchCount == 2) {
			if (!CamZoom) {
				CamZoom = true;
				var rayA = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
				var rayB = Camera.main.ScreenPointToRay (Input.GetTouch (1).position);
				int layerMask = (1 << 8);
				if (Physics.Raycast (rayA, out hit, 1500, layerMask)) {
					PrevA = new Vector3 (hit.point.x, 0, hit.point.z);
					Debug.Log ("PrevA: " + PrevA);
				}
				if (Physics.Raycast (rayB, out hit, 1500, layerMask)) {
					PrevB = new Vector3 (hit.point.x, 0, hit.point.z);
					Debug.Log ("PrevB: " + PrevB);
				}
				PrevDis = Vector3.Distance (PrevB, PrevA);
				Debug.Log ("PrevDis: " + PrevDis);
				PrevScaleV = new Vector3 (PrevScale, PrevScale, PrevScale);
				PrevScale = this.transform.localScale.x;
				Debug.Log ("PrevScale: " + PrevScale);
			}
			if (CamZoom) {
				var rayA = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
				var rayB = Camera.main.ScreenPointToRay (Input.GetTouch (1).position);
				int layerMask = (1 << 8);
				if (Physics.Raycast (rayA, out hit, 1500, layerMask)) {
					NewA = new Vector3 (hit.point.x, 0, hit.point.z);
				}
				if (Physics.Raycast (rayB, out hit, 1500, layerMask)) {
					NewB = new Vector3 (hit.point.x, 0, hit.point.z);
				}
				DeltaDis = PrevDis - (Vector3.Distance (NewB, NewA));
				Debug.Log ("Delta: " + DeltaDis);
				NewScale = PrevScale + (DeltaDis / 1000);
				Debug.Log ("NewScale: " + NewScale);
				NewScaleV = new Vector3 (NewScale, NewScale, NewScale);
				this.transform.localScale = Vector3.Lerp(PrevScaleV,NewScaleV,Time.deltaTime);
				PrevScaleV = NewScaleV;
				CamAngle();
			}
		}
	}

	private void CamAngle(){
		if (NewScale <= 0.25) {
			CurrentAngle = MinAngle + (NewScale - 0.01f) * (MaxAngle - MinAngle) / (0.25f - 0.01f);
			this.transform.GetChild(0).transform.localEulerAngles = new Vector3 (CurrentAngle, 0, 0);
		} else {
			this.transform.GetChild(0).transform.localEulerAngles = new Vector3 (45f, 0, 0);
		}
	}
	
	public void OnFingerUp(Lean.LeanFinger finger)
	{
		CamActive = false;
		CamZoom = false;
	}
*/
}