﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Level_Stats : MonoBehaviour {
	private Level_Load LoadLevel;
	private UI_Controller ui;
	private GameObject ui_lock;
	private GameObject ui_sheep_lock;
	private GameObject ui_level_number;
	private GameObject ui_stars;
	private Text ui_sheep_min;
	private SheepCount sCount;
	private int levelnum = 0;

	void Awake () {
		//Debug.Log ("Level_Stats.cs Awake()");
		sCount = GameObject.Find ("Chunk Manager").GetComponent<SheepCount> ();
		LoadLevel = GameObject.Find ("Chunk Manager").GetComponent<Level_Load> ();
		ui = GameObject.Find ("UI_Controller").GetComponent<UI_Controller> ();
		//Debug.Log ("END Level_Stats.cs Awake()");
	}

	void Start () {
		//Debug.Log ("Level_Stats.cs Start()");
		//this.transform.GetComponent<Button>().onClick.AddListener(() => LevelCheck(levelnum));
		//Debug.Log ("END Level_Stats.cs Start()");
	}

	public void LevelCheck(){
		//Debug.Log ("--- BEGIN LevelCheck ---");
		//Debug.Log ("Level: " + level);
		bool access = LoadLevel.GetLevelAccess(levelnum);
		Debug.Log ("Access: " + access);
		if (access == true){
			LoadLevel.LastLevel = levelnum;
			LoadLevel.LoadLevel (levelnum);
			LoadLevel.StartLevel ();
		} else {
			ui.GetComponent<UI_Button_Anim>().Wiggle(GameObject.Find("Menu_level").transform.GetChild(0).gameObject);
		}
		//Debug.Log ("--- END LevelCheck ---");
	}

	public void level_number (){
		//Debug.Log ("level_number()");
		int name = int.Parse(this.gameObject.name.Substring(6));
		//Debug.Log ("Level: " + name);
		levelnum = name;
		//Debug.Log ("END level_number()");
	}

	public int min_sheep (){
		//Debug.Log ("min_sheep()");
		int sheep = LoadLevel.Levels[levelnum].MinSheep;
		//Debug.Log ("END min_sheep()");
		return (sheep);
	}

	public int level_lock (){
		//Debug.Log ("level_lock()");
		int locknum = LoadLevel.UnlockStats[levelnum].Lock;
		//Debug.Log ("END level_lock()");
		return (locknum);
	}

	public void UIUnlockStats () {
		//Debug.Log ("UIUnlockStats()");
		ui_level_number = this.transform.FindChild ("ui_level_number").gameObject;
		ui_stars = this.transform.FindChild ("ui_stars").gameObject;
		ui_lock = this.transform.FindChild ("ui_level_lock").gameObject;
		ui_sheep_lock = this.transform.FindChild ("ui_sheep_lock").gameObject;
		ui_sheep_min = ui_sheep_lock.transform.GetChild (0).GetComponent<Text>();

		ui_level_number.GetComponent<Text> ().text = levelnum.ToString ();
		RefreshStars ();
		ui_sheep_min.text = min_sheep ().ToString ();

		int Lock = LoadLevel.UnlockStats[levelnum].Lock;
		if (Lock == 1) {
			ui_lock.SetActive (true);
			ui_sheep_lock.SetActive (false);
			ui_level_number.SetActive (false);
			ui_stars.SetActive (false);
		} else {
			ui_lock.SetActive (false);
			ui_level_number.SetActive (true);
			ui_stars.SetActive (true);
			RefreshSheepLock();
		}

		this.GetComponent<RectTransform>().localPosition = new Vector3 ((470 * (levelnum))+150, 0, 0);
		//Debug.Log ("END UIUnlockStats()");
	}

	public void RefreshStars(){
		//Debug.Log ("RefreshStars()");
		//LoadLevel.SetStars (levelnum, LoadLevel.UnlockStats [levelnum].Stars, ui_stars);
		int Stars = LoadLevel.UnlockStats[levelnum].Stars;
		switch (Stars) {
		case 0:
			ui_stars.transform.GetChild (0).gameObject.SetActive (false);
			ui_stars.transform.GetChild (1).gameObject.SetActive (false);
			ui_stars.transform.GetChild (2).gameObject.SetActive (false);
			ui_stars.transform.GetChild (3).gameObject.SetActive (true);
			ui_stars.transform.GetChild (4).gameObject.SetActive (true);
			ui_stars.transform.GetChild (5).gameObject.SetActive (true);
			break;
		case 1:
			ui_stars.transform.GetChild (0).gameObject.SetActive (true);
			ui_stars.transform.GetChild (1).gameObject.SetActive (false);
			ui_stars.transform.GetChild (2).gameObject.SetActive (false);
			ui_stars.transform.GetChild (3).gameObject.SetActive (false);
			ui_stars.transform.GetChild (4).gameObject.SetActive (true);
			ui_stars.transform.GetChild (5).gameObject.SetActive (true);
			break;
		case 2:
			ui_stars.transform.GetChild (0).gameObject.SetActive (true);
			ui_stars.transform.GetChild (1).gameObject.SetActive (true);
			ui_stars.transform.GetChild (2).gameObject.SetActive (false);
			ui_stars.transform.GetChild (3).gameObject.SetActive (false);
			ui_stars.transform.GetChild (4).gameObject.SetActive (false);
			ui_stars.transform.GetChild (5).gameObject.SetActive (true);
			break;
		case 3:
			ui_stars.transform.GetChild (0).gameObject.SetActive (true);
			ui_stars.transform.GetChild (1).gameObject.SetActive (true);
			ui_stars.transform.GetChild (2).gameObject.SetActive (true);
			ui_stars.transform.GetChild (3).gameObject.SetActive (false);
			ui_stars.transform.GetChild (4).gameObject.SetActive (false);
			ui_stars.transform.GetChild (5).gameObject.SetActive (false);
			break;
		}
		//Debug.Log ("END RefreshStars()");
	}

	public void RefreshSheepLock(){
		//Debug.Log ("RefreshSheepLock()");
		if (sCount.FlockCount < int.Parse (ui_sheep_min.text) && LoadLevel.UnlockStats[levelnum].Lock !=1) {
			ui_sheep_lock.SetActive (true);
		} else {
			ui_sheep_lock.SetActive (false);
		}
		//Debug.Log ("END RefreshSheepLock()");
	}

	public void CheckLock (){
		//Debug.Log ("CheckLock()");
		ui_level_number = this.transform.FindChild ("ui_level_number").gameObject;
		ui_stars = this.transform.FindChild ("ui_stars").gameObject;
		ui_lock = this.transform.FindChild ("ui_level_lock").gameObject;
		ui_sheep_lock = this.transform.FindChild ("ui_sheep_lock").gameObject;
		ui_sheep_min = ui_sheep_lock.transform.GetChild (0).GetComponent<Text>();
		if (levelnum != LoadLevel.UnlockStats.Count - 1) {
			LoadLevel.UnlockStats[levelnum].Lock = 0;
		}
		if (LoadLevel.UnlockStats[levelnum].Lock == 1) {
			ui_lock.SetActive (true);
			ui_sheep_lock.SetActive (false);
			ui_level_number.SetActive (false);
			ui_stars.SetActive (false);
		} else {
			ui_lock.SetActive (false);
			ui_level_number.SetActive (true);
			ui_stars.SetActive (true);
			RefreshSheepLock();
		}
		//Debug.Log ("END CheckLock()");
	}

	public void NewLevel(){
		//Debug.Log ("NewLevel()");
		level_number ();
		UIUnlockStats ();
		CheckLock ();
		//Debug.Log ("END NewLevel()");
	}

}
