﻿using UnityEngine;
using System.Collections;

public class Trap : MonoBehaviour {
	private bool Triggered;

	public void TriggerTrap () {
		if (transform.name.Substring(0,8) == "BearTrap") {
			transform.GetChild(0).GetComponent<BearTrapS> ().ActivateTrap();
		}
		if (transform.name == "Trigger_Dragon") {
			this.GetComponent<DragonTrigger> ().enabled = true;
		}
		Triggered = true;
	}

	public bool getTriggered () {
		return Triggered;
	}
}
