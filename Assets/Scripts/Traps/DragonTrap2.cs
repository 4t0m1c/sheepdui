﻿using UnityEngine;
using System.Collections;

public class DragonTrap2 : MonoBehaviour {

	public Transform Target;
	public Transform DragonRoot;
	public Transform Dragon;
	//public Transform Flames;
	public Transform DragonShadowRoot;
	public Transform DragonShadow;
	public Transform DragonRotate;
	public float fadeTime = 3;
	public GameObject flamesPrefab;
	float EatDelay = 10;
	Vector3 newPos;
	Vector3 originPos;
	float startTime;
	float journeyLength;
	IEnumerator FadeCoroutine;
	Material mat;
	Vector3 EndPos = Vector3.zero;
	float closein = 6;
	float FlySpeed;
	bool kill = false;
	public bool fade = false;
	bool activateFade = true;

	void Start () {
		//Set Dragon Parts
		originPos = this.transform.position;
		DragonRoot = this.transform;
		DragonRotate = DragonRoot.GetChild(0);
		DragonShadowRoot = DragonRotate.GetChild(0);
		Dragon = DragonShadowRoot.GetChild(0);
		//Flames = Dragon.GetChild(0);
		DragonShadow = DragonShadowRoot.GetChild(1);
		mat = DragonShadow.GetComponent<SpriteRenderer>().material;
		FlySpeed = Random.Range (15, 35);
		FadeCoroutine = Fade (false, fadeTime);
		StartCoroutine (FadeCoroutine);
		//FindTarget();
	}

	void Update () {
		CheckStatus();
	}

	void CheckStatus(){
		//Debug.Log ("Chasing Target");
		if (fade) {
			DragonShadowRoot.Translate (Vector3.forward * (Time.deltaTime * 3));
			if (activateFade){
				activateFade = false;
				StopCoroutine (FadeCoroutine);
				FadeCoroutine = Fade (true, fadeTime);
				StartCoroutine (FadeCoroutine);
			}
		} else {
			if (Target != null) {
				FlyAround (); //chasing target
				if (Vector3.Distance (transform.position, originPos) < 10) {
					//Debug.Log ("Circling Target");
					CircleTarget ();
				} else {
					Target = null;
					StopCoroutine (FadeCoroutine);
					FadeCoroutine = Fade (true, fadeTime);
					StartCoroutine (FadeCoroutine);
				}
			}
		}
	}

	void FlyAround(){
		if (Target != null){
			if (kill != true){
				newPos = Target.position;
				transform.position = Vector3.MoveTowards(transform.position,newPos,0.1f);
			}
		}
	}

	void CircleTarget(){
		if (closein > 3){
			DragonRotate.Rotate(Vector3.down * (Time.deltaTime * FlySpeed),Space.Self);
			closein = DragonShadowRoot.localPosition.x-(Time.deltaTime*0.31f);
			closein = Mathf.Clamp(closein,2,Mathf.Infinity);
			DragonShadowRoot.localPosition = new Vector3(closein,closein,closein);
		} else {
			KillTarget();
		}
	}

	IEnumerator Fade (bool FadeOut = false, float FadeTime = 1f) {
		Color MatColour = mat.color;
		float R = MatColour.r;
		float G = MatColour.g;
		float B = MatColour.b;
		float CalculatedTime = 1 / (FadeTime * 100);
		
		if (FadeOut) {
			for (float f = 1f; f >= 0; f -= CalculatedTime) {
				mat.color = (new Color(R, G, B, f));
				Debug.Log(f);
				//DragonShadowRoot.position = DragonShadowRoot.position + ((-EndDir.normalized* Time.deltaTime)*2);
				//DragonShadowRoot.GetComponent<Animator> ().enabled = false;
				yield return new WaitForEndOfFrame();
			}
			Destroy (this.gameObject);
		} else {
			for (float f = 0f; f <= 1f; f += CalculatedTime) {
				mat.color = (new Color(R, G, B, f));
				Debug.Log(f);
				yield return new WaitForEndOfFrame();
			}
		}

		StopCoroutine (FadeCoroutine);
		yield return null;
	}

	void KillTarget(){
		if (!fade) {
			if (EndPos == Vector3.zero) {
				EndPos = Target.position;
				EndPos.y = DragonShadowRoot.position.y;
				//Quaternion lookat = Quaternion.LookRotation (EndPos, Vector3.up);
				DragonShadowRoot.rotation = Quaternion.LookRotation (EndPos, Vector3.up);//Quaternion.Euler(DragonShadowRoot.rotation.eulerAngles-lookat.eulerAngles);
				Debug.Log ("EndPos Direction: " + EndPos);
				kill = true;
			}
			DragonShadowRoot.Translate (Vector3.forward * (Time.deltaTime * 3));
			//DragonShadowRoot.position = DragonShadowRoot.position + ((-EndDir.normalized* Time.deltaTime)*2);
			Debug.Log ("DragonShadowRoot Pos: " + DragonShadowRoot.position);
			Debug.Log ("Orientation: "+new Vector3 (EndPos.x, DragonShadowRoot.localRotation.y, EndPos.z));

			if (DragonShadowRoot.GetComponent<Animator> ().enabled == false) {
				DragonShadowRoot.GetComponent<Animator> ().enabled = true;
				GameObject flames = Instantiate (flamesPrefab);
				flames.transform.SetParent (Dragon);
				flames.transform.localPosition = new Vector3 (0, 1, 0.3f);
				flames.transform.localRotation = Quaternion.Euler (-10f, 0, 90);
			}

			if (Vector3.Distance (DragonShadowRoot.position, Target.position) <= 1f) {
				Target.GetComponent<Sheep> ().Die (true);

			}
		}
	}

}
