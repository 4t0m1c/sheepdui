﻿using UnityEngine;
using System.Collections;

public class DragonTrigger: MonoBehaviour {

	public GameObject DragonPrefab;
	public GameObject[] Dragons;
	private DragonTrap2 Target;
	private GameObject t;

	public void Update () {
		//Get Closest Sheep
		Dragons = GameObject.FindGameObjectsWithTag("Trap_Dragon");
		if (Dragons.Length == 0 || Dragons.Length == null){
			GameObject[] sheepz = GameObject.FindGameObjectsWithTag("Sheep");
			float distance = Mathf.Infinity;
			for (int i = 0; i<sheepz.Length; i++) {
				float newDistance = Vector3.Distance(transform.position, sheepz[i].transform.position);
				if (newDistance < distance) {
					t = sheepz[i];
					distance = newDistance;
				}
			}
			if (distance < 1) { //Sheep has activated block
				//Activate Dragon
				GameObject Dragon = Instantiate(DragonPrefab);
				Target = Dragon.GetComponent<DragonTrap2>();
				Target.Target = t.transform;
				Dragon.transform.position = this.transform.position;
				Destroy(this);
				}
		}
	}
}
