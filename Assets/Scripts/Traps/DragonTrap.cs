﻿using UnityEngine;
using System.Collections;

public class DragonTrap : MonoBehaviour {

	public GameObject Target;
	public GameObject DragonRoot;
	public GameObject Dragon;
	public GameObject DragonShadowRoot;
	public GameObject DragonShadow;
	public GameObject DragonRotate;
	float EatDelay = 10;
	Vector3 newPos;
	float startTime;
	float journeyLength;
	bool circleTrigger = false;

	void Start () {
		//Set Dragon Parts
		DragonRoot = this.transform.gameObject;
		DragonRotate = DragonRoot.transform.GetChild(0).gameObject;
		Dragon = DragonRotate.transform.GetChild(1).gameObject;
		DragonShadowRoot = DragonRotate.transform.GetChild(0).gameObject;
		DragonShadow = DragonShadowRoot.transform.GetChild(0).gameObject;
	}

	void Update () {
		CheckStatus();
	}

	void CheckStatus(){
		GameObject[] sheepz = GameObject.FindGameObjectsWithTag("Sheep");
		if (EatDelay > 0){
			EatDelay -= Time.deltaTime;
			Debug.Log (EatDelay);
			FlyAround();
		} else if (sheepz.Length != 0 && EatDelay <= 0 && Target == null){
			Debug.Log ("Hunting Time");
			FindTarget();
		} else if (sheepz.Length != 0 && EatDelay <= 0 && Target != null){
			Debug.Log ("Chasing Target");
			FlyAround();
			if (transform.position == Target.transform.position){
				Debug.Log ("Circling Target");
				circleTrigger = true;
			}
			if (circleTrigger){
				CircleTarget();
			}
		} else {
			Debug.Log ("Just Flying Around");
			FlyAround();
		}
	}

	void FlyAround(){
		if (Target == null){
			//Returns ShadowRoot back to correct offset
			if (DragonShadowRoot.transform.localPosition.x < 5f){
				DragonShadowRoot.transform.localPosition = new Vector3(DragonShadowRoot.transform.localPosition.x+0.01f,DragonShadowRoot.transform.localPosition.y+0.01f,DragonShadowRoot.transform.localPosition.z+0.01f);
			} else if (DragonShadowRoot.transform.localPosition.x >= 5f){
				DragonShadowRoot.transform.localPosition = new Vector3(5,5,5);
			}
			//Rotate back to correct angle
			//TODO fix this
			if (Mathf.Round(DragonRotate.transform.localRotation.y) == 0 || Mathf.Round(DragonRotate.transform.localRotation.y) == 360){
				DragonRotate.transform.localRotation = Quaternion.Euler(0,0,0);
			} else if (DragonRotate.transform.localRotation.y < 0){
				DragonRotate.transform.localRotation = Quaternion.Euler(0,DragonRotate.transform.localRotation.y-0.01f,0);
			} else if (DragonRotate.transform.localRotation.y > 0){
				DragonRotate.transform.localRotation = Quaternion.Euler(0,DragonRotate.transform.localRotation.y+0.01f,0);
			}
			//Return Dragon
			if (Dragon.transform.localPosition.y != 30){
				Dragon.transform.localPosition = new Vector3 (0,Dragon.transform.localPosition.y+1f,0);
			} else if (Mathf.Round(Dragon.transform.localPosition.y)==30){
				Dragon.transform.localPosition = new Vector3 (0,30,0);
			}
			//Reset circleTrigger;
			circleTrigger = false;
		}
		if (Target != null){
			newPos = Target.transform.position;
		} else if (newPos == null || transform.position == newPos){
			int x = Random.Range (-35, 35);
			int z = Random.Range (-20, 20);
			newPos = new Vector3(x,0,z);
		}
		transform.position = Vector3.MoveTowards(transform.position,newPos,0.01f);
	}

	void FindTarget () {
		Debug.Log ("FindTarget ()");
		GameObject[] sheepz = GameObject.FindGameObjectsWithTag("Sheep");
		Debug.Log ("Searched for sheep from "+sheepz.Length+" sheep");
		float distance = 0;
		float newDistance = 0;
		for (int i = 0; i<sheepz.Length; i++) {
			for (int j = 0; j<sheepz.Length; j++) {
				newDistance = Vector3.Distance(sheepz[i].transform.position, sheepz[j].transform.position);
				if (newDistance >= distance) {
					Target = sheepz[j];
					Debug.Log ("Target Sheep: " + Target);
					startTime = Time.time;
					distance = newDistance;
				}
			}
		}
	}

	void CircleTarget(){
		Debug.Log ("Rotating Dragon");
		DragonRotate.transform.Rotate(Vector3.down * (Time.deltaTime * 20),Space.Self);
		float closein = DragonShadowRoot.transform.localPosition.x-(Time.deltaTime*0.31f);
		closein = Mathf.Clamp(closein,0,Mathf.Infinity);
		Debug.Log("CloseIn: "+closein);
		if (closein != 0){
			DragonShadowRoot.transform.localPosition = new Vector3(closein,closein,closein);
		} else {
			KillTarget();
		}
	}

	void KillTarget(){
		//TODO Add kill animation in here
		if (Dragon.transform.position != Target.transform.position){
			Dragon.transform.position = Vector3.MoveTowards(Dragon.transform.position,Target.transform.position,0.5f);
		} else {
			Target.GetComponent<Sheep> ().Die(true);
			EatDelay = 30;
		}
	}

}
