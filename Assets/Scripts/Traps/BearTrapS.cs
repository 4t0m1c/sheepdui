﻿using UnityEngine;
using System.Collections;

public class BearTrapS : MonoBehaviour {
	public Sprite TriggerSprite;
	public Animator TriggerAnimation;
	public bool Trigger = false;

	void Start () {
		TriggerAnimation = GetComponent<Animator> ();
	}

	public void ActivateTrap () {
		TriggerAnimation.SetTrigger ("Activated");
	}

	void Update () {
		if (Trigger) {
			GameObject[] sheepz = GameObject.FindGameObjectsWithTag("Sheep");
			
			float distance = Mathf.Infinity;
			int nearest = 0;
			for (int i = 0; i<sheepz.Length; i++) {
				float newDistance = Vector3.Distance(transform.position, sheepz[i].transform.position);
				if (newDistance < distance) {
					nearest = i;
					distance = newDistance;
				}
			}
			if (distance < 1) {
				sheepz[nearest].GetComponent<Sheep> ().Die(true);
			}
			Destroy(TriggerAnimation);
			transform.GetComponent<SpriteRenderer> ().sprite = TriggerSprite;
			Destroy(this);
		}
	}
}
