﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

public class InputTest : MonoBehaviour {
	Camera mainCam;
	Light spot;
	MoveController Mover;
	public Canvas Canvas;
	// Use this for initialization
	void Start () {
		mainCam = Camera.main;
		spot = transform.GetChild (1).GetComponent<Light>();
		Mover = transform.GetComponent<MoveController>();
		//Canvas = GameObject.Find ("Canvas").GetComponent<Canvas> ();
	}

	// Update is called once per frame
	void Update () {
		int fingerCount = Input.touches.Length;

		if (fingerCount != 0) {
			Touch touch = Input.GetTouch (0);

			bool isUI = TestOverUI(touch.position);

			if (touch.phase == TouchPhase.Began) {
				Mover.Started(touch.position, isUI);
			}
			
			if (touch.phase == TouchPhase.Moved) {
				Mover.Moved(touch.position, isUI);
			}
			
			if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) {
				Mover.Ended(touch.position, isUI);
			}
		}
		#if UNITY_EDITOR 
		mainCam.orthographicSize -= Input.GetAxis("Mouse ScrollWheel") * 2;
		spot.spotAngle -= Input.GetAxis("Mouse ScrollWheel") * 20;

		if (Input.GetMouseButtonDown(0)) {
			if (Mover.getAddS()) {
				Mover.AddSheep(Input.mousePosition);
			} else {
				Mover.CommandSheep (Input.mousePosition);
			}
		}

		float x = Input.GetAxis("Horizontal") * 0.1f;
		float z = Input.GetAxis("Vertical") * 0.1f;
		Vector3 result = new Vector3(x, 0, z);
		result = Quaternion.AngleAxis(45, Vector3.up) * result;

		transform.Translate(result);
		#endif
	}

	private bool TestOverUI (Vector2 touchPos) {
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		eventDataCurrentPosition.position = touchPos;
		
		GraphicRaycaster uiRaycaster = Canvas.gameObject.GetComponent<GraphicRaycaster>();
		List<RaycastResult> results = new List<RaycastResult>();
		uiRaycaster.Raycast(eventDataCurrentPosition, results);
		return results.Count > 0;
	}
}