﻿using UnityEngine;
using System.Collections;

public class UI_Button_Anim : MonoBehaviour {

	public GameObject LeftArrow;
	public GameObject RightArrow;

	public void ScaleRightButtonDown(GameObject RightArrow){
		iTween.ScaleTo(RightArrow, iTween.Hash("name","RightArrow","scale",new Vector3(1.3f,1.3f,1f),"time",0.1f,"easetype","easeInOutQuart"));
	}
	
	public void ScaleLeftButtonDown(GameObject LeftArrow){
		iTween.ScaleTo(LeftArrow, iTween.Hash("name","LeftArrow","scale",new Vector3(1.3f,1.3f,1f),"time",0.1f,"easetype","easeInOutQuart"));
	}
	
	public void ScaleRightButtonUp(GameObject RightArrow){
		iTween.ScaleTo(RightArrow, iTween.Hash("name","RightArrow","scale",new Vector3(1f,1f,1f),"time",0.2f,"easetype","easeInOutQuart"));
	}
	
	public void ScaleLeftButtonUp(GameObject LeftArrow){
		iTween.ScaleTo(LeftArrow, iTween.Hash("name","LeftArrow","scale",new Vector3(1f,1f,1f),"time",0.2f,"easetype","easeInOutQuart"));
	}

	public void Wiggle(GameObject Element){
		iTween.PunchRotation (Element, iTween.Hash("name","wiggle","amount",new Vector3 (0, 0, -30),"time", 0.5f));
	}

}
