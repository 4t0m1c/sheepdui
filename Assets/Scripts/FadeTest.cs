﻿using UnityEngine;
using System.Collections;

public class FadeTest : MonoBehaviour {

	Material mat;
	IEnumerator FadeCoroutine;
	
	void Start () {
		mat = GetComponent<SpriteRenderer>().material;
		FadeCoroutine = Fade (true, 1f);
		StartCoroutine (FadeCoroutine);
	}

	IEnumerator Fade (bool FadeOut = false, float FadeTime = 1f) {
		Color MatColour = mat.color;
		float R = MatColour.r;
		float G = MatColour.g;
		float B = MatColour.b;
		float CalculatedTime = 1 / (FadeTime * 1000);
		
		if (FadeOut) {
			for (float f = 1f; f >= 0; f -= CalculatedTime) {
				mat.color = (new Color(R, G, B, f));
				Debug.Log(f);
				yield return new WaitForEndOfFrame();
			}
		} else {
			for (float f = 0f; f <= 1f; f += CalculatedTime) {
				mat.color = (new Color(R, G, B, f));
				Debug.Log(f);
				yield return new WaitForEndOfFrame();
			}
		}
		StopCoroutine (FadeCoroutine);
		yield return null;
	}
}
