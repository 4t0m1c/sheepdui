﻿using UnityEngine;
using System.Collections;

public class UI_IAP_Slave : MonoBehaviour {

	// Use this for initialization
	void OnEnable () {
		GameObject.Find("IAP_Canvas").GetComponent<UI_IAP>().ActiveTier(this.gameObject);
		Debug.Log("Sent Tier Info");
	}

}
