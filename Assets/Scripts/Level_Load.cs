﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;

public class LevelStats
{
	public int Level { get; set; }
	public int Lock { get; set; }
	public int Stars { get; set; }
	
}

// Stored in a directory called Levels this class holds the data for each level. The list of data is saved as if your manipulating an array from the editor.
// It is then packaged with the build. Player prefs holds your current max level and XML data holds stars.
public class LevelData
{
	public int DensityMultiplier;
	public int TrapDensity;
	public int MinSheep;

	public int BearTrap;
	public int Anvil;
	public int LargeWeight;
	public int Spikes;
	public int SawMill;
	public int FalseBlock;
	public int Wolf;
	public int Dragon;

	public LevelData (int level) {
		DensityMultiplier = 1;
		TrapDensity = Mathf.RoundToInt(((0.02f*level)/(1+0.02f*level))*100);
		MinSheep = (1*level)+1;

		BearTrap = 18;
		Anvil = 18;
		LargeWeight = 18;
		Spikes = 18;
		SawMill = 18;
		FalseBlock = 5;
		Wolf = 5;
		Dragon = 1;
	}
}

public class Level_Load : MonoBehaviour {
	public bool Ready = false;
	public int LevelMax;
	public List<LevelStats> UnlockStats;

	public int LastLevel = -1;
	private SheepCount sCount;
	private ChunkManager Manager;
	private UI_Controller ui;
	private StartEnd startEnd;
	private int MinSheepTmp;

	public int[] keys;
	public int[] DataDensityMultiplier;
	public int[] DataTrapDensity;
	public int[] DataMinSheep;
	public int[] DataBearTrap;
	public int[] DataAnvil;
	public int[] DataLargeWeight;
	public int[] DataSpikes;
	public int[] DataSawMill;
	public int[] DataFalseBlock;
	public int[] DataWolf;
	public int[] DataDragon;
	public Dictionary<int, LevelData> Levels;

	void Awake () {
		Levels = new Dictionary<int, LevelData> ();
		if (keys.Length != 0) {
			for (int i = 0; i < keys.Length; i++) {
				LevelData data = new LevelData (keys [i]);
				
				data.DensityMultiplier = DataDensityMultiplier [i];
				data.TrapDensity = DataTrapDensity [i];
				
				data.BearTrap = DataBearTrap [i];
				data.Anvil = DataAnvil [i];
				data.LargeWeight = DataLargeWeight [i];
				data.Spikes = DataSpikes [i];
				data.SawMill = DataSawMill [i];
				data.FalseBlock = DataFalseBlock [i];
				data.Wolf = DataWolf [i];
				data.Dragon = DataDragon [i];
				
				Levels.Add (keys [i], data);
			}
		}

		//Debug.Log ("################################################################################");
		//Debug.Log ("--- BEGIN START ---");
		// Find my friends
		Manager = transform.GetComponent<ChunkManager> ();
		sCount = transform.GetComponent<SheepCount> ();
		startEnd = transform.GetComponent<StartEnd> ();
		ui = GameObject.Find ("UI").transform.GetChild(0).GetComponent<UI_Controller> ();

		if (!PlayerPrefs.HasKey("FlockCount")) {
			PlayerPrefs.SetInt ("FlockCount", 1);
		}

		// Read Player Prefs -- Test if there are any
		if (!PlayerPrefs.HasKey ("LevelMax")) {
			// Set a default max level
			ResetStats();
		} else {
			// Fetch the data found in player prefs
			LevelMax = PlayerPrefs.GetInt ("LevelMax");
			LoadStats();
		}

		//Debug.Log ("--- END START ---");
		//Debug.Log ("################################################################################");
	}


	// Use this to reload the game data in settings
	public void ResetStats() {
		LevelMax = 3;
		PlayerPrefs.SetInt ("LevelMax", LevelMax);

		UnlockStats = new List<LevelStats> ();

		UnlockStats.Add (new LevelStats () {Level=0, Lock=0, Stars=0});
		UnlockStats.Add (new LevelStats () {Level=1, Lock=0, Stars=0});
		UnlockStats.Add (new LevelStats () {Level=2, Lock=0, Stars=0});
		UnlockStats.Add (new LevelStats () {Level=3, Lock=1, Stars=0});
		sCount.FlockCount = -1;
		PlayerPrefs.SetInt ("FlockCount",sCount.FlockCount);
		SaveStats ();

		ui.LoadLevels (UnlockStats);
	}

	// Loads the data from XML if it cant find the file it loads the defaults from reset
	public void LoadStats() {
		if (File.Exists (Application.persistentDataPath + Path.DirectorySeparatorChar + "settings_level.txt")) {
			UnlockStats = DansCSharpLibrary.Serialization.XmlSerialization.ReadFromXmlFile<List<LevelStats>> (Application.persistentDataPath + Path.DirectorySeparatorChar + "settings_level.txt");
		
			ui.LoadLevels (UnlockStats);
		} else {
			ResetStats();
		}
	}

	// Save XML data
	public void SaveStats(){
		DansCSharpLibrary.Serialization.XmlSerialization.WriteToXmlFile<List<LevelStats>> (Application.persistentDataPath + Path.DirectorySeparatorChar + "settings_level.txt", UnlockStats, false);
	}

	// Starts the level provided
	public void LoadLevel(int level) {
		// TODO Sort out trap density in the LevelData Class & anything else thats missing here.

		//TODO Give buttons their own command script for lock/unlock/load/SetStars (these should not contain an Update() function). 
		//Tell them to unlock and update stars through ui.LoadLevels.
		//Tell them load a level just call this function from their script.
		if (Levels.ContainsKey(level) == false) {
			Levels.Add(level, new LevelData(level));
		}

		MinSheepTmp = Levels [level].MinSheep;
		Manager.TrapSpawnDensity = Levels [level].TrapDensity;

		Manager.TrapSpawnChance [0] = Levels [level].BearTrap;
		Manager.TrapSpawnChance [1] = Levels [level].Anvil;
		Manager.TrapSpawnChance [2] = Levels [level].LargeWeight;
		Manager.TrapSpawnChance [3] = Levels [level].Spikes;
		Manager.TrapSpawnChance [4] = Levels [level].SawMill;
		Manager.TrapSpawnChance [5] = Levels [level].FalseBlock;
		Manager.TrapSpawnChance [6] = Levels [level].Wolf;
		Manager.TrapSpawnChance [7] = Levels [level].Dragon;

		Manager.LoadRandomTrapList ();
	}

	public void StartLevel () {
		Manager.Seed = Mathf.RoundToInt(Random.Range(1111,9999)*Time.time);
		//Manager.Seed = 5060;
		Manager.ResetWorld ();
		ui.Play (GameObject.Find("Menu_main"));
		startEnd .StartMap (MinSheepTmp);
	}

	// Provide completed level number and star count acheived
	public void CompleteLevel(int level, int stars) {
		// TODO A script to send a completed level command needs to be made and connected to the end goal + Cheat button (See Level_Load.CompleteLevel below)
		// It should pick the star count and send the data here.

		// Sets completed levels stars
		UnlockStats [level].Stars = stars;
		RectTransform ui_container = ui.Menu_NextLevel.GetComponent<RectTransform> ();
		GameObject thislevel = ui_container.FindChild ("level_" + level.ToString ()).gameObject;
		thislevel.GetComponent<Level_Stats>().RefreshStars();

		// Tests to see if new levels have been unlocked. Please double check that I got the Max level check rigt.

		//if (level + 2 == LevelMax) {
		if (UnlockStats[UnlockStats.Count-2].Stars > 0) {
			// Add level stats
			UnlockStats [UnlockStats.Count-1].Lock = 0;
			LevelMax += 1;
			PlayerPrefs.SetInt ("LevelMax", LevelMax);
			UnlockStats.Add (new LevelStats () {Level=UnlockStats.Count, Lock=1, Stars=0});
		}

		SaveStats ();
		ui.LoadLevels (UnlockStats);
	}
	
	public bool GetLevelAccess (int Lvl){
		//Debug.Log ("--- BEGIN GetLevelAccess ---");
		bool value = false;
		//Level_Get (Lvl);
		//Debug.Log ("Checking Access...");
		//Debug.Log ("Test " + levelstats.Count);
		if (UnlockStats [Lvl].Lock == 0) {
			//Notification about lock
			if (sCount.FlockCount >= Levels[Lvl].MinSheep) {
				value = true;
			}
		} else {
			value = false;
		}
		//Debug.Log (value);
		//Debug.Log ("--- END GetLevelAccess ---");
		return value;
	}

	/*public void LoadLevel (int level_num) {
		Debug.Log ("--- BEGIN LoadLevel ---");
		Level_Get (level_num);
		LastLevel = level_num;

		Manager.TrapSpawnChance[0] = Mathf.RoundToInt(Levels.TryGetValue);
		Manager.TrapSpawnChance[1] = Mathf.RoundToInt(Level [2]);
		Manager.TrapSpawnChance[2] = Mathf.RoundToInt(Level [3]);
		Manager.TrapSpawnChance[3] = Mathf.RoundToInt(Level [4]);
		Manager.TrapSpawnChance[4] = Mathf.RoundToInt(Level [5]);
		Manager.TrapSpawnChance[5] = Mathf.RoundToInt(Level [6]);
		Manager.TrapSpawnChance[6] = Mathf.RoundToInt(Level [7]);
		Manager.TrapSpawnChance[7] = Mathf.RoundToInt(Level [8]);
		Manager.TrapSpawnDensity = Mathf.RoundToInt(Level [0]);

		Random.seed = Mathf.RoundToInt(Time.time + Random.Range(0, 99999));
		Manager.Seed = Mathf.RoundToInt(Random.Range(0, 99999));
		Debug.Log ("Seed: " + Manager.Seed);

		Manager.ResetWorld ();
		Debug.Log ("--- END LoadLevel ---");
	}

	public void SetStars(int level_num, int Stars, GameObject gStars){
		Debug.Log ("--- BEGIN SetStars ---");
		switch (Stars) {
		case 0:
			gStars.transform.GetChild (0).gameObject.SetActive (false);
			gStars.transform.GetChild (1).gameObject.SetActive (false);
			gStars.transform.GetChild (2).gameObject.SetActive (false);
			gStars.transform.GetChild (3).gameObject.SetActive (true);
			gStars.transform.GetChild (4).gameObject.SetActive (true);
			gStars.transform.GetChild (5).gameObject.SetActive (true);
			break;
		case 1:
			gStars.transform.GetChild (0).gameObject.SetActive (true);
			gStars.transform.GetChild (1).gameObject.SetActive (false);
			gStars.transform.GetChild (2).gameObject.SetActive (false);
			gStars.transform.GetChild (3).gameObject.SetActive (false);
			gStars.transform.GetChild (4).gameObject.SetActive (true);
			gStars.transform.GetChild (5).gameObject.SetActive (true);
			break;
		case 2:
			gStars.transform.GetChild (0).gameObject.SetActive (true);
			gStars.transform.GetChild (1).gameObject.SetActive (true);
			gStars.transform.GetChild (2).gameObject.SetActive (false);
			gStars.transform.GetChild (3).gameObject.SetActive (false);
			gStars.transform.GetChild (4).gameObject.SetActive (false);
			gStars.transform.GetChild (5).gameObject.SetActive (true);
			break;
		case 3:
			gStars.transform.GetChild (0).gameObject.SetActive (true);
			gStars.transform.GetChild (1).gameObject.SetActive (true);
			gStars.transform.GetChild (2).gameObject.SetActive (true);
			gStars.transform.GetChild (3).gameObject.SetActive (false);
			gStars.transform.GetChild (4).gameObject.SetActive (false);
			gStars.transform.GetChild (5).gameObject.SetActive (false);
			break;
		}
		Debug.Log ("--- END SetStars ---");
	}*/

}