﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(ChunkManager))]
public class ChunkManagerInspector : Editor {
	private ChunkManager TargetScript;

	// Use this for initialization
	public void OnEnable () {
		TargetScript = (ChunkManager)target;
	}
	
	public override void OnInspectorGUI() {
		//base.OnInspectorGUI ();
		serializedObject.Update();

		EditorGUILayout.BeginVertical ();

		EditorGUILayout.BeginVertical ();
		EditorGUILayout.BeginHorizontal ();
		if ( GUILayout.Button ("RESET")) {
			if (Application.isEditor) {
				if (Application.isPlaying) {
					TargetScript.ResetWorld();
				}
			}
		}
		EditorGUILayout.EndHorizontal ();
		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Display All Chunks");
		TargetScript.LoadAll = EditorGUILayout.Toggle (TargetScript.LoadAll);
		EditorGUILayout.EndHorizontal ();
		EditorGUILayout.LabelField ("Chunk Generation Controls");
		TargetScript.Size = EditorGUILayout.Slider ("World Size", TargetScript.Size, 1, 100);
		TargetScript.myResolution = EditorGUILayout.IntSlider ("Resolution", TargetScript.myResolution, 8, 64);
		TargetScript.PerlinScale = EditorGUILayout.Slider ("Perlin Scale", TargetScript.PerlinScale, 1, 100);
		TargetScript.HeightMultiplier = EditorGUILayout.Slider ("Heightmap Multiplier", TargetScript.HeightMultiplier, 1, 100);
		TargetScript.DrawDistance = EditorGUILayout.IntField ("Draw Distance", TargetScript.DrawDistance);
		TargetScript.Seed = EditorGUILayout.IntField ("Seed", TargetScript.Seed);
		EditorGUILayout.EndVertical ();

		EditorGUILayout.BeginVertical ();
		EditorGUILayout.Space ();
		EditorGUILayout.LabelField ("Terrain Art:");
		TargetScript.TerrainSpawnDensity = EditorGUILayout.IntField ("Terrain Spawn Density", TargetScript.TerrainSpawnDensity);
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("TerrainPrefabs"), true);
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("TerrainSpawnChance"), true);

		EditorGUILayout.LabelField ("Trap Objects:");
		TargetScript.TrapSpawnDensity = EditorGUILayout.IntField ("Trap Spawn Density", TargetScript.TrapSpawnDensity);
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("TrapPrefabs"), true);
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("TrapSpawnChance"), true);
		EditorGUILayout.EndVertical ();

		EditorGUILayout.EndVertical ();

		if (GUI.changed) {
			EditorUtility.SetDirty(TargetScript);
			serializedObject.ApplyModifiedProperties();

			if (Application.isEditor) {
				if (Application.isPlaying) {
					TargetScript.ResetWorld();
				}
			}
		}
	}
}
