﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class LevelManager : EditorWindow {
	int curLevel = 0;
	int lastLevel;
	Level_Load levelLoader;
	LevelData currentData;
	Dictionary<int, LevelData> tempDirectory;

	// Add menu named "My Window" to the Window menu
	[MenuItem ("Window/Level Manager")]
	static void Init () {
		// Get existing open window or if none, make a new one:
		LevelManager window = (LevelManager)EditorWindow.GetWindow (typeof (LevelManager));
		window.Show();
	}

	void OnGUI () {
		if (tempDirectory == null) {
			dataInit();
		}

		if (lastLevel != curLevel) {
			loadData();
			lastLevel = curLevel;
		}

		EditorGUILayout.BeginVertical ();
		// Set the current Level
		EditorGUILayout.LabelField ("Level", EditorStyles.boldLabel);
		EditorGUILayout.BeginHorizontal ();
		curLevel = EditorGUILayout.IntField ("Level Number", curLevel);
		if(GUILayout.Button("↑")){curLevel++;}
		if(GUILayout.Button("↓")){if (curLevel != 0){curLevel--;}}
		EditorGUILayout.EndHorizontal ();
		EditorGUILayout.Space();
		EditorGUILayout.LabelField ("Level Settings", EditorStyles.boldLabel);

		// Get/Set Values
		currentData.TrapDensity = EditorGUILayout.IntSlider ("Trap Density", currentData.TrapDensity, 0, 100);
		currentData.MinSheep = EditorGUILayout.IntField ("Min Sheep", currentData.MinSheep);
		EditorGUILayout.Space();
		EditorGUILayout.Space();

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Bear Trap", EditorStyles.boldLabel);
		currentData.BearTrap = EditorGUILayout.IntField(currentData.BearTrap);
		EditorGUILayout.LabelField (("1:" + GetChance(currentData.BearTrap) + " chance"), EditorStyles.boldLabel);
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Anvil", EditorStyles.boldLabel);
		currentData.Anvil = EditorGUILayout.IntField(currentData.Anvil);
		EditorGUILayout.LabelField (("1:" + GetChance(currentData.Anvil) + " chance"), EditorStyles.boldLabel);
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Large Weight", EditorStyles.boldLabel);
		currentData.LargeWeight = EditorGUILayout.IntField(currentData.LargeWeight);
		EditorGUILayout.LabelField (("1:" + GetChance(currentData.LargeWeight) + " chance"), EditorStyles.boldLabel);
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Spikes", EditorStyles.boldLabel);
		currentData.Spikes = EditorGUILayout.IntField(currentData.Spikes);
		EditorGUILayout.LabelField (("1:" + GetChance(currentData.Spikes) + " chance"), EditorStyles.boldLabel);
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Saw Mill", EditorStyles.boldLabel);
		currentData.SawMill = EditorGUILayout.IntField(currentData.SawMill);
		EditorGUILayout.LabelField (("1:" + GetChance(currentData.SawMill) + " chance"), EditorStyles.boldLabel);
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("False Block", EditorStyles.boldLabel);
		currentData.FalseBlock = EditorGUILayout.IntField(currentData.FalseBlock);
		EditorGUILayout.LabelField (("1:" + GetChance(currentData.FalseBlock) + " chance"), EditorStyles.boldLabel);
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Wolf", EditorStyles.boldLabel);
		currentData.Wolf = EditorGUILayout.IntField(currentData.Wolf);
		EditorGUILayout.LabelField (("1:" + GetChance(currentData.Wolf) + " chance"), EditorStyles.boldLabel);
		EditorGUILayout.EndHorizontal ();

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Dragon", EditorStyles.boldLabel);
		currentData.Dragon = EditorGUILayout.IntField(currentData.Dragon);
		EditorGUILayout.LabelField (("1:" + GetChance(currentData.Dragon) + " chance"), EditorStyles.boldLabel);
		EditorGUILayout.EndHorizontal ();
		EditorGUILayout.Space();
		EditorGUILayout.Space();

		// Save, Reset and Reset all buttons
		EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Reset All")) {
			ResetAll();
		}
		if (GUILayout.Button ("Reset Current")) {
			ResetCurrent();
		}
		if (GUILayout.Button ("Save Current")) {
			SaveData();
		}
		EditorGUILayout.EndHorizontal ();
		EditorGUILayout.EndVertical ();
	}
	
	int GetChance (int value) {
		int total = currentData.Anvil + currentData.LargeWeight + currentData.Spikes + currentData.SawMill + currentData.FalseBlock + currentData.Wolf + currentData.Dragon;
		if (value == 0) {
			return 0;} 
		else {
			return Mathf.RoundToInt (total/value);}
	}

	void loadData () {
		if (tempDirectory.ContainsKey (curLevel)) {
			currentData = tempDirectory[curLevel];
		} else if ((levelLoader.keys != null)) {
			tempDirectory.Clear();
			for (int i = 0; i < levelLoader.keys.Length; i++) {
				LevelData data = new LevelData (levelLoader.keys [i]);
				
				data.DensityMultiplier = levelLoader.DataDensityMultiplier [i];
				data.TrapDensity = levelLoader.DataTrapDensity [i];
				
				data.BearTrap = levelLoader.DataBearTrap [i];
				data.Anvil = levelLoader.DataAnvil [i];
				data.LargeWeight = levelLoader.DataLargeWeight [i];
				data.Spikes = levelLoader.DataSpikes [i];
				data.SawMill = levelLoader.DataSawMill [i];
				data.FalseBlock = levelLoader.DataFalseBlock [i];
				data.Wolf = levelLoader.DataWolf [i];
				data.Dragon = levelLoader.DataDragon [i];
				
				tempDirectory.Add (levelLoader.keys [i], data);
			}
			if (tempDirectory.ContainsKey(curLevel)) {
				currentData = tempDirectory[curLevel];
			} else {
				currentData = new LevelData(curLevel);
			}
		} else {
			currentData = new LevelData(curLevel);
		}
	}

	void SaveData () {
		if (tempDirectory.ContainsKey(curLevel)) {
			tempDirectory.Remove (curLevel);
		}
		tempDirectory.Add (curLevel, currentData);
		WriteData ();
	}

	void WriteData () {
		int[] Keys = new int[tempDirectory.Count];
		int[] DataDensityMultiplier = new int[tempDirectory.Count];
		int[] DataTrapDensity = new int[tempDirectory.Count];
		int[] DataMinSheep = new int[tempDirectory.Count];
		int[] DataBearTrap = new int[tempDirectory.Count];
		int[] DataAnvil = new int[tempDirectory.Count];
		int[] DataLargeWeight = new int[tempDirectory.Count];
		int[] DataSpikes = new int[tempDirectory.Count];
		int[] DataSawMill = new int[tempDirectory.Count];
		int[] DataFalseBlock = new int[tempDirectory.Count];
		int[] DataWolf = new int[tempDirectory.Count];
		int[] DataDragon = new int[tempDirectory.Count];
		
		
		tempDirectory.Keys.CopyTo (Keys, 0);
		for (int i=0; i<tempDirectory.Count; i++) {
			DataDensityMultiplier[i] = tempDirectory[Keys[i]].DensityMultiplier;
			DataTrapDensity[i] = tempDirectory[Keys[i]].TrapDensity;
			DataMinSheep[i] = tempDirectory[Keys[i]].MinSheep;
			DataBearTrap[i] = tempDirectory[Keys[i]].BearTrap;
			DataAnvil[i] = tempDirectory[Keys[i]].Anvil;
			DataLargeWeight[i] = tempDirectory[Keys[i]].LargeWeight;
			DataSpikes[i] = tempDirectory[Keys[i]].Spikes;
			DataSawMill[i] = tempDirectory[Keys[i]].SawMill;
			DataFalseBlock[i] = tempDirectory[Keys[i]].FalseBlock;
			DataWolf[i] = tempDirectory[Keys[i]].Wolf;
			DataDragon[i] = tempDirectory[Keys[i]].Dragon;
		}
		
		levelLoader.keys = Keys;
		levelLoader.DataDensityMultiplier = DataDensityMultiplier;
		levelLoader.DataTrapDensity = DataTrapDensity;
		levelLoader.DataMinSheep = DataMinSheep;
		levelLoader.DataBearTrap = DataBearTrap;
		levelLoader.DataAnvil = DataAnvil;
		levelLoader.DataLargeWeight = DataLargeWeight;
		levelLoader.DataSpikes = DataSpikes;
		levelLoader.DataSawMill = DataSawMill;
		levelLoader.DataFalseBlock = DataFalseBlock;
		levelLoader.DataWolf = DataWolf;
		levelLoader.DataDragon = DataDragon;
	}

	void ResetCurrent () {
		currentData = new LevelData (curLevel);
		tempDirectory.Remove (curLevel);
		WriteData ();
	}

	void ResetAll () {
		tempDirectory = new Dictionary<int, LevelData> (curLevel);
		curLevel = 0;
		currentData = new LevelData (curLevel);

		levelLoader.keys = null;
		levelLoader.DataDensityMultiplier = null;
		levelLoader.DataTrapDensity = null;
		levelLoader.DataMinSheep = null;
		levelLoader.DataBearTrap = null;
		levelLoader.DataAnvil = null;
		levelLoader.DataLargeWeight = null;
		levelLoader.DataSpikes = null;
		levelLoader.DataSawMill = null;
		levelLoader.DataFalseBlock = null;
		levelLoader.DataWolf = null;
		levelLoader.DataDragon = null;
	}

	void dataInit () {
		tempDirectory = new Dictionary<int, LevelData> ();
		currentData = new LevelData (curLevel);
		levelLoader = GameObject.Find("Chunk Manager").GetComponent<Level_Load>();

		loadData ();
		lastLevel = curLevel;
	}
}