﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class ForceDotNet4 : Editor {
	static ForceDotNet4 () {
		EditorApplication.update += RunOnce;
	}

	static void RunOnce()
	{
		//Debug.Log("Setting .Net version to 4.0");
		EditorApplication.update -= RunOnce;
		
		UpgradeSolutions ();
	}

	static void UpgradeSolutions()
	{
		string currentDir = Directory.GetCurrentDirectory();
		string[] slnFile = Directory.GetFiles(currentDir, "*.sln");
		string[] csprojFile = Directory.GetFiles(currentDir, "*.csproj");
		List<string> formatUpdates = new List<string>();
		List<string> toolsUpdates = new List<string>();
		List<string> frameworkUpdates = new List<string>();
		
		if (slnFile != null)
		{
			for (int i = 0; i < slnFile.Length; i++)
			{
				if (ReplaceInFile(slnFile[i], "Format Version 10.00", "Format Version 11.00"))
				{
					formatUpdates.Add(Path.GetFileNameWithoutExtension(slnFile[i]));
				}
			}
		}
		
		if (csprojFile != null)
		{
			for (int i = 0; i < csprojFile.Length; i++)
			{
				if (ReplaceInFile(csprojFile[i], "ToolsVersion=\"3.5\"", "ToolsVersion=\"4.0\""))
				{
					toolsUpdates.Add(Path.GetFileNameWithoutExtension(csprojFile[i]));
				}
				
				if (ReplaceInFile(csprojFile[i], "<TargetFrameworkVersion>v3.5</TargetFrameworkVersion>", "<TargetFrameworkVersion>v4.0</TargetFrameworkVersion>"))
				{
					frameworkUpdates.Add(Path.GetFileNameWithoutExtension(csprojFile[i]));
				}
			}
		}
	}

	static private bool ReplaceInFile(string filePath, string searchText, string replaceText)
	{
		StreamReader reader = new StreamReader(filePath);
		string content = reader.ReadToEnd();
		reader.Close();
		if (content.IndexOf(searchText) != -1)
		{
			content = Regex.Replace(content, searchText, replaceText);
			StreamWriter writer = new StreamWriter(filePath);
			writer.Write(content);
			writer.Close();
			return true;
		}
		
		return false;
	}
}
